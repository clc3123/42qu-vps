#!/usr/bin/env python
# -*- coding: utf-8 -*-
from _host_user import HOST_USER_CONFIG
from _env import PREFIX

import re
from mako.template import Template
from os import mkdir
from os.path import join, dirname, exists, isdir
from misc.config.render import RENDER_PATH


_CONFIG_DIR = join(PREFIX, 'misc/config/nginx')
with open(join(dirname(__file__), 'nginx.conf')) as conf:
    tmpl = conf.read()
T = Template(tmpl)


for host, user, config in HOST_USER_CONFIG:
    CONFIG_DIR = join(_CONFIG_DIR, host)

    if not exists(CONFIG_DIR):
        mkdir(CONFIG_DIR)

    port_site_range = range(config.PORT_SITE, config.PORT_SITE+config.PROCESS_NUM_SITE)
    port_rpc_range = range(config.PORT_RPC, config.PORT_RPC+config.PROCESS_NUM_RPC)
    port_api_range = range(config.PORT_API, config.PORT_API+config.PROCESS_NUM_API)

    conf = T.render(
        host=config.HOST,
        name=config.HOST.replace('.', '_'),
        port_site_range=port_site_range,
        port_rpc_range=port_rpc_range,
        port_api_range=port_api_range,
        prefix=PREFIX,
        host_css_js=config.HOST_CSS_JS,
        host_dev_prefix=config.HOST_DEV_PREFIX,
        root_path=RENDER_PATH[0],
    )

    with open(join(CONFIG_DIR, '%s.conf' % user), 'w') as f:
        f.write(conf)


print('created nginx configure files at %s' % _CONFIG_DIR)

