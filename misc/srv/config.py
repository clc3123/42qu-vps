#coding:utf-8
import _env


class VM_DOMAIN_STATE():
    UNRECORD = 10
    WAIT_VERIFY = 20
    RECORDING = 30
    RECORDED = 40

MYSQL_HOST = '127.0.0.1'
MYSQL_PORT = '3306'
MYSQL_USER = 'jack'
MYSQL_PASSWD = 'awdz!@#'
MYSQL_PREFIX = 'jack'
MYSQL_DATEBASE= 'jack_vps'
DOMAIN_TABLE = 'VmDomain'

#NGINX_CONFIG_FILE = '/etc/nginx/nginx.conf'
NGINX_CONFIG_FILE = '/home/jack/nginx.conf'
