#coding:utf-8
import envoy
import  MySQLdb 
import tornado.ioloop
import tornado.httpserver
from tornado.wsgi import WSGIContainer
from glob import glob
from os import chmod , remove
from update_thread import UpdateThread
from os.path import basename,abspath, dirname, join
from DBUtils.PersistentDB import PersistentDB as DB
from config import *

def WSGIServer(port, application):
    container = WSGIContainer(application)
    http_server = tornado.httpserver.HTTPServer(container)
    http_server.listen(port)
    tornado.ioloop.IOLoop.instance().start()

def application(environ, start_response):
    status = '200 OK'
    path = environ['PATH_INFO'].lstrip('/')
    if path.isdigit():
        host_proxy_refresh(path)

    response_headers = [('Content-type', 'text/plain')]
    write = start_response(status, response_headers)
    return ['()']

def connection(*args, **kwds):
    kwds['maxusage'] = False
    persist = DB (MySQLdb, *args, **kwds)
    conn = persist.connection()
    return conn 

def connect(host, user, passwd, db, **kwargs):
    conn_params = dict(host=host, user=user,
                        db=db, init_command='set names utf8',
                        **kwargs)
    conn_params['passwd'] = passwd
    conn = connection(**conn_params)
    return conn

def domain_list():
    db = connect(host=MYSQL_HOST,user=MYSQL_USER,
                passwd=MYSQL_PASSWD,db=MYSQL_DATEBASE)
    cursor = db.cursor()
    cursor.execute("SELECT id, vm_id, domain, ip FROM %s WHERE state = %s"%(DOMAIN_TABLE, VM_DOMAIN_STATE.UNRECORD))
    rows = cursor.fetchall()
    cursor.close()
    db.close()
    return rows 

def restart():
    NGINX_CONF = join(abspath('.'), 'nginx.conf')
    #print NGINX_CONF
    CMD_MV = 'sudo mv %s  %s'%(NGINX_CONF, NGINX_CONFIG_FILE)
    print CMD_MV 
    r = envoy.run(CMD_MV)
    print r.std_out
    print r.std_err
    print('created nginx configure files at %s' %NGINX_CONFIG_FILE)
    CMD_RESTART_NGINX = 'sudo /etc/init.d/nginx reload'
    print CMD_MV 
    c = envoy.run(CMD_RESTART_NGINX)
    print c.std_out
    print c.std_err

def host_proxy_refresh(path):
    conf = []
    for i in domain_list():
        TEMPLATE = """
server {
    listen      80;
    server_name %(host)s *.%(host)s;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header REMOTE-HOST $remote_addr;
    proxy_set_header HOST $host.42qu.com;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    location / {
        proxy_pass http://%(ip)s;
    }
}
        """
        conf.append(TEMPLATE%{"host":i[2], "ip":i[3]})

    with open(join(dirname(__file__), 'nginx.conf'), 'w') as f:
        for i in conf:
            tmp = f.write(i)

    t=UpdateThread(restart, restart.__name__)
    t.start()

if __name__ == '__main__':
    WSGIServer(42421, application)
