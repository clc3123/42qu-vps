#coding:utf-8
import _env
from model.vps.domain import VmDomain, domain_bind_new
from model.vps.ip import VpsIp, ip2int
from model.vps.const import VM_DOMAIN_STATE 

domain_list = [] 
f = open('nginx.conf')
for i in f:
    list = i.split()
    if len(list) >2:
        for x in xrange(len(list)-2):
            domain_list.append([list[x], list[-1]])
    else:
        domain_list.append([list[0], list[1]])

for i  in domain_list:
    ip = VpsIp.get(ip=ip2int(i[1]))
    if ip:
        domain_bind_new(ip.vm_id, i[0], i[1], 0,  VM_DOMAIN_STATE.UNRECORD)
    else:
        print i[0],i[1]
        domain_bind_new(0, i[0], i[1], 0,  VM_DOMAIN_STATE.UNRECORD)

if __name__=='__main__':
    pass
