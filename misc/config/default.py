#coding:utf-8
import _env
from os.path import dirname, join

def pre_config(o):
    from _table import MYSQL_CONFIG
 
    o.MYSQL_CONFIG = MYSQL_CONFIG
    
    o.SSL_CERT = join(_env.PREFIX,'misc/config/vps/private/server.pem')
    o.SAAS_PORT = 50042
    try:
        import private.saas
        o.ALLOWED_IPS = private.saas.ALLOWED_IPS
    except ImportError:
        pass
    

def post_config(o):
    pass

