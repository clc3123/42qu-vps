#coding:utf-8

import _env
from model.pay.pay import PayOrder
from model.pay.const import PAY_ORDER_STATE, PAY_GOODS_CN
from model.vps.vm import Vm
from model.vps.template import VmTemplate
from model.vps.bill import bill_month_new, vm_bill_day_new
from misc.lib.base.single_process import single_process
from misc.lib.base.timetool import today_days, month_begin, month_begin_second, month_end, ONE_DAY_SECOND
from model.base.gid import CID
from model._db import redis, redis_key
from time import time
import collections

#PayOrder
#id    cid  rid     cent    address_id  time    from_id     to_id   state 

VM_BILL_CENT_MONTH = collections.defaultdict(int)
VM_BILL_VOLUME_MONTH = collections.defaultdict(int)
VM_BILL_CENT_DAY = collections.defaultdict(int)
VM_BILL_VOLUME_DAY = collections.defaultdict(int)

def get_day_data(order_list):
    days = today_days()

    for order in order_list:
        vm = Vm.mc_get(order.rid)
        VM_BILL_DAY_KEY = (vm.country_id, vm.data_center_id, vm.vm_template_id, order.cid) 

        VM_BILL_CENT_DAY[VM_BILL_DAY_KEY] += order.cent
        VM_BILL_VOLUME_DAY[VM_BILL_DAY_KEY] += 1 

    for k, v in VM_BILL_CENT_DAY.iteritems():
        volume = VM_BILL_VOLUME_DAY[k] 
        k = list(k)
        #print k[0],k[1],k[2],k[3], volume,days 
        vm_bill_day_new(k[0],k[1],k[2],k[3], volume, v, days)

    VM_BILL_CENT_DAY.clear()
    VM_BILL_VOLUME_DAY.clear()

def get_month_data(order_list):
    month = month_begin()

    for order in order_list:
        vm = Vm.mc_get(order.rid)
        for i in (
            (CID.BILL_COUNTRY, vm.country_id),
            (CID.BILL_DATA_CENTER, vm.data_center_id),
            (CID.BILL_TEMPLATE, vm.vm_template_id),
            (CID.BILL_GOODS_CID, order.cid),
        ):
            VM_BILL_CENT_MONTH[i] += order.cent
            VM_BILL_VOLUME_MONTH[i] += 1 

    for k, v in VM_BILL_CENT_MONTH.iteritems():
        volume = VM_BILL_VOLUME_MONTH[k]
        k = list(k)
        #print k[0], k[1], v, volume, days
        bill_month_new(k[0], k[1], v, volume, month)

    VM_BILL_CENT_MONTH.clear()
    VM_BILL_VOLUME_MONTH.clear()

def pay_day_bills():
    order_list = PayOrder.where(
        'time >= %s and state >= %s and (cid = %s or cid = %s)', 
        int(time()) -ONE_DAY_SECOND, 
        PAY_ORDER_STATE.WAIT_SEND,
        CID.VPS,
        CID.VPS_VM_RENEW
    )

    get_day_data(order_list)

def pay_month_bills():
    order_list = PayOrder.where(
        'time >= %s and state >= %s and (cid = %s or cid = %s)', 
        month_begin_second(),
        PAY_ORDER_STATE.WAIT_SEND,
        CID.VPS,
        CID.VPS_VM_RENEW
    )

    get_month_data(order_list)


@single_process
def main():
    pay_day_bills()
    pay_month_bills()

if __name__ == '__main__':
    main()

