#coding:utf-8
import _env
from misc.lib.base.timetool import today_days
from model.base.user_mail import mail_by_user_id
from model.base.sendmail_srv import render_mail
from model.vps.const import VM_STATE
from model.vps.vm import Vm
from misc.lib.base.single_process import single_process
from model.vps.mail import remind_mail_list

def vps_one_expired():
    days = today_days() 
    expired_id_list = Vm.where('date_end <= %s'%days, state_=VM_STATE.OPEN)

    for vm in expired_id_list:
        vm.state = VM_STATE.CLOSE
        vm.save ()
        user_id = vm.user_id 
        
        mail_list = [mail_by_user_id(user_id)]
        _mail_list = remind_mail_list(vm.id)
        for mail_id, mail in _mail_list:
            if mail_id > 0: 
                mail_list.append(mail)

        for mail in mail_list:
            render_mail(
                '/vps/_mail/expired.html',
                'VPS (编号:%s) 因到期被关闭'%vm.id,
                mail,
                id = vm.id 
            )



def vps_one_expire_warning():
    days = today_days()

    for i in (1, 5, 10):
        expiring_id_list = Vm.where('date_end = %s'%(days+i), state_=VM_STATE.OPEN)
        for vm in expiring_id_list:
            user_id = vm.user_id 

            mail_list = [mail_by_user_id(user_id)]
            _mail_list = remind_mail_list(vm.id)
            for mail_id, mail in _mail_list:
                if mail_id > 0: 
                    mail_list.append(mail)

            for mail in mail_list:
                render_mail(
                    '/vps/_mail/expire_warning.html',
                    'VPS(编号：%s) 续费提醒 ( %s天后到期 )'%(vm.id,i),
                    mail,
                    id=vm.id,
                    warning_days= i
                 )

@single_process
def main():
    vps_one_expired()
    vps_one_expire_warning()

if __name__ == '__main__':
    main()
