#coding:utf-8
import _env

from model.base.god import GOD_NAV
from model.base.gid import CID
from misc.config import HOST
NAV = (
    ("//vps.%s/god/bill"%HOST, "报表", None, None),
    ("//vps.%s/god/vm"%HOST, "虚拟机", None, None),
    ("//vps.%s/god/datacenter"%HOST, "IDC", None, None),
)

GOD_NAV[(CID.VPS, '主机')] = NAV


