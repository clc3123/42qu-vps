#!/usr/bin/env python
# coding:utf-8

import _env
import collections
from model.base.gid import CID

VPS_HOST_TYPE_NAME2ID = collections.OrderedDict (
    centos6_xen= 1,
    centos5_xen=2,
    ubuntu_xen_openvswitch=3,
)

VPS_HOST_TYPE_ID2NAME = dict()

for k, v in VPS_HOST_TYPE_NAME2ID.iteritems():
    VPS_HOST_TYPE_ID2NAME[v] = k

class VM_STATE:
    RM = 0
    RESERVE = 5
    PAY = 10
    OPEN = 15
    CLOSE = 20

VM_STATE_CN = collections.OrderedDict()
VM_STATE_CN[VM_STATE.OPEN] = '运行中'
VM_STATE_CN[VM_STATE.PAY] = '待开通'
VM_STATE_CN[VM_STATE.CLOSE] = '被关闭'
VM_STATE_CN[VM_STATE.RESERVE]  = '未付款'
VM_STATE_CN[VM_STATE.RM]  = '已删除'


class REMIND_MAIL_STATE():
    WAIT_VERIFY = 10
    DONE = 20
    DELETED = 0

REMIND_MAIL = collections.OrderedDict()
REMIND_MAIL[REMIND_MAIL_STATE.WAIT_VERIFY] = '待验证,点此重新验证'
REMIND_MAIL[REMIND_MAIL_STATE.DONE] = '删除'
REMIND_MAIL[REMIND_MAIL_STATE.DELETED] = '已删除'

BILL_GOODS = {}


BILL_CLASS = collections.OrderedDict()
BILL_CLASS[CID.BILL_COUNTRY] = '国家'
BILL_CLASS[CID.BILL_DATA_CENTER] = '机房'
BILL_CLASS[CID.BILL_TEMPLATE] = '模版'
BILL_CLASS[CID.BILL_GOODS_CID] = '交易类型'

class Bill(object):
    CID = None
    BILL_GOODS = ''
    def __init__(self, id):
        self.id = id

CID_BILL = {} 

class VM_DOMAIN_STATE(object):
    UNRECORD = 10
    WAIT_VERIFY = 20
    RECORDING = 30
    RECORDED = 40

VM_DOMAIN= collections.OrderedDict()
VM_DOMAIN[VM_DOMAIN_STATE.UNRECORD] = '未备案'
VM_DOMAIN[VM_DOMAIN_STATE.WAIT_VERIFY] = '待认证'
VM_DOMAIN[VM_DOMAIN_STATE.RECORDING] = '备案中'
VM_DOMAIN[VM_DOMAIN_STATE.RECORDED] = '已备案'

class VM_IP_TYPE():
    HAS_IP_INT_ONLY = 0
    HAS_IP_EXT = 1 
