#!/usr/bin/env python
# coding:utf-8

import _env
from model._db import Model , ModelMc, redis, redis_key, McLimitA, McNum

#
#CREATE TABLE IF NOT EXISTS `VmHardDisk` (
#  `id` bigint(20) NOT NULL AUTO_INCREMENT,
#  `vm_id` bigint(20) NOT NULL,
#  `hd_id` int(11) NOT NULL COMMENT '每台Vm自增',
#  `size` int(11) NOT NULL COMMENT 'unit G',
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `vm_id` (`vm_id`,`hd_id`)
#) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

class VmHardDisk (Model):

    @property    
    def device_name (self):
        if self.hd_id == 0:
            return "/dev/xvda1"
        else:
            return "/dev/xvdc%s" % (self.hd_id)

    @property
    def mount_point (self):
        if self.hd_id == 0:
            return "/"
        else:
            return "/mnt/data%s" % (self.hd_id)

def vm_harddisk_new (vm_id, size, hd_id=None):
    if hd_id is None:
        row = VmHardDisk.execute("select max(hd_id) from VmHardDisk where vm_id=%s", vm_id).fetchone()
        hd_id = row[0]
        if hd_id is None:
            hd_id = 0
        else:
            hd_id = int (hd_id) + 1
    disk = VmHardDisk ()
    disk.vm_id = vm_id
    disk.size = size
    disk.hd_id = int (hd_id)
    disk.save ()

def vm_harddisk_list (vm_id):
    return VmHardDisk.where (vm_id=vm_id)

def vm_harddisk_new_default (vm_id, hd_total):
    if hd_total <= 30:
        vm_harddisk_new (vm_id, hd_total, 0)
    else:
        vm_harddisk_new (vm_id, 15, 0)
        vm_harddisk_new (vm_id, hd_total - 15, 1)


def vm_harddisk_delete (vm_id, hd_id=None):
    if hd_id is None:
        disks = VmHardDisk.where (vm_id=vm_id)
        for disk in disks:
            disk.delete ()
    disk = VmHardDisk.get (vm_id=vm_id, hd_id=hd_id)
    if disk:
        disk.delete ()

def vm_harddisk_upgrade (vm_id, hd_total_new):
    disks = VmHardDisk.where (vm_id=vm_id)
    if len (disks) == 1 and hd_total_new <= 30:
        disks[0].delete ()
        vm_harddisk_new (vm_id, hd_total_new, 0)
    else:
        total_size = 0
        for disk in disks:
            total_size += disk.size
        if hd_total_new > total_size:
            vm_harddisk_new (vm_id, hd_total_new - total_size)


if __name__ == '__main__':
    #vm_harddisk_delete (15, None)
    #vm_harddisk_new (15, 400)

    #vm_harddisk_new (1, 10)
    pass

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 :
