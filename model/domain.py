#!/usr/bin/env python
# coding:utf-8

import _env
from model._db import Model , ModelMc, redis, redis_key
from zweb.attrcache import attrcache
from model.vps.const import *
from model.vps.vm import Vm 
from model.vps.ip import vm_ip_list


class VmDomain(ModelMc):

    @attrcache
    def user_id(self):
        vm = Vm.mc_get(self.vm_id)
        return vm.user_id

    def can_admin(self, user_id):
        if user_id:
            return int(self.user_id) == int(user_id)

#id  vm_id domain ip record

def domain_bind_new(vm_id, domain, record, state):
    vm_domain = VmDomain()
    vm_domain.vm_id = vm_id
    vm_domain.domain = domain
    ip_list = vm_ip_list(vm_id)
    vm_domain.ip= ip_list[0]
    vm_domain.record = record
    vm_domain.state = state
    vm_domain.save()
    

def domain_rm_if_can(id, user_id):
    domain= VmDomain.mc_get(id)
    if not domain:
        return False
    if domain.can_admin(user_id):
        domain.delete()

def domain_list(vm_id): 
    _domain_list = list(VmDomain.where(vm_id=vm_id))
    return _domain_list


if __name__ == '__main__':
    #l = domain_list (77)
    #for f in l:
    #    print f.domain, f.ip, f.state
    #domain_rm_if_can(1,10000026)
    pass
