#coding:utf-8
import _env
from model.base.sendmail_srv import render_mail
from model.vps.mail import get_active_user_mail

def mail_disk_error (vps_id, mail_address, op_time):
    render_mail (
        '/vps/_mail/disk_error.html',
        '物理机维护通知',
        mail_address,
        id=vps_id,
        op_time=op_time,
    )

def mail_sys_upgrade (vps_id, mail_address, op_time):
    render_mail (
        '/vps/_mail/sys_upgrade.html',
        '物理机维护通知',
        mail_address,
        id=vps_id,
        op_time=op_time,
    )

def mail_ddos (idc, mail_address, op_time):
    render_mail (
        '/vps/_mail/ddos.html',
        '故障通告',
        mail_address,
        idc=idc,
        op_time=op_time,
    )

def mail_ddos2 (idc, mail_address, op_time):
    render_mail (
        '/vps/_mail/ddos2.html',
        '故障通告',
        mail_address,
        idc=idc,
        op_time=op_time,
    )



def mail_idc_ddos (data_center_id, data_center_name, op_time):
    vps_dict = get_active_user_mail (data_center_id=data_center_id)
    user_dict = dict ()
    for mail_address in vps_dict.values ():
        user_dict[mail_address] = 1
    for mail_address in user_dict.keys ():
        mail_ddos (data_center_name, mail_address, op_time)


def mail_idc_ddos2 (data_center_id, data_center_name, op_time):
    vps_dict = get_active_user_mail (data_center_id=data_center_id)
    user_dict = dict ()
    for mail_address in vps_dict.values ():
        user_dict[mail_address] = 1
    for mail_address in user_dict.keys ():
        mail_ddos2 (data_center_name, mail_address, op_time)


MAIL_GOOGLE_GROUP = "42qu-vps-order@googlegroups.com"

if __name__ == '__main__':
    vps_dict = get_active_user_mail (data_center_id=2)
    print ",".join (vps_dict.values ())
    pass
    #mail_idc_ddos (2, "石景山", "2012-09-26 12:00-15:30")
    #mail_sys_upgrade (1, "frostyplanet@gmail.com", "2012-09-26 0:00-0:30")
    #vps_dict = get_active_user_mail (host_id=5)
    #for vps_id, mail_address in vps_dict.iteritems ():
    #    mail_disk_error (vps_id, mail_address, "2012-09-26 0:00-0:30")
    #    mail_disk_error (vps_id, MAIL_GOOGLE_GROUP, "2012-09-26 0:00-0:30")

