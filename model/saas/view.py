#coding:utf-8
import _env
from _saas.ttypes import CMD, Vps, Ip
from model.vps.saas.cmd import cmd_get, cmd_done
from model.vps.vm import Vm, vm_delete_done
from model.vps.const import VM_STATE
from model.base.sendmail import sendmail
from model.vps.host import vps_host_update
from model.vps.ip import VpsIp
from model.vps.vm_harddisk import VmHardDisk
import logging

class View(object):
    def todo(self, host_id, cmd):
        r = cmd_get(host_id, cmd)
        return r

    def done(self, host_id, cmd, id, state, message):
        _count = cmd_done(host_id, cmd, id, state, message)
        sendmail(
            '42qu-vps-saas@googlegroups.com',
            'cmd_done(host_id=%s, cmd=%s, id=%s, state=%s, message=%s) = %s'%(
                host_id, CMD._VALUES_TO_NAMES.get(cmd, '?'), id, state, message, _count
            ),
            '',
        )
        if cmd == CMD.OPEN:
            if state == 0:
                vm = Vm.mc_get (id)
                if vm:
                    vm.state = VM_STATE.OPEN
                    vm.save ()
        if cmd == CMD.RM:
            if state == 0:
                vm_delete_done (id, host_id)
#
#
#    def plot(self, cid, rid, value):
#        plot( cid, rid, value)
#
    def netflow_save(self, host_id, netflow, timestamp):
        pass
#        netflow_save(host_id, netflow, timestamp)
#
#    def sms(self, number_list, txt):
#        for i in number_list:
#            sms_send_mq(i, txt)        
#
    def host_refresh(self, host_id, hd_remain, ram_remain, hd_total=0, ram_total=0):
        vps_host_update (host_id, hd_remain=hd_remain, ram_remain=ram_remain, hd_total=hd_total, ram_total=ram_total)
#
    def vps(self, vm_id):
        try:
            vm = Vm.get(vm_id)
            if not vm:
                return Vps()

            ips = VpsIp.where (vm_id=vm_id)
            disks = VmHardDisk.where (vm_id=vm_id)
            vps = Vps(
                id=vm.id,
                password=vm.password,
                os=vm.os_id,
                cpu=vm.cpu,
                ram=vm.ram,
                host_id = vm.host_id, 
                state=vm.state,
                bandwidth=vm.bandwidth,
            )
            vps.int_ip=Ip ()
            vps.ext_ips = []
            vps.harddisks = {} 
            for ip in ips:
                vps.ext_ips.append (Ip (ipv4=ip.ip, ipv4_netmask=ip.netmask, mac=ip.mac))
            if ips:
                vps.gateway = Ip (ipv4=ips[0].gateway, ipv4_netmask=ips[0].netmask, mac="")
            for disk in disks:
                vps.harddisks[disk.hd_id] = disk.size
            return vps
        except Exception, e:
            logging.exception (e)
            return Vps ()
#
if __name__ == '__main__':
    pass

    view = View()
    print view.todo(2, CMD.OPEN)
    print view.done( 2, CMD.OPEN, 3, 3, "测试")
