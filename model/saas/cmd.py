#coding:utf-8
import _env
import sys
from model._db import redis
from _saas.ttypes import CMD
from array import array
from time import time

REDIS_VPS_SAAS_CMD = 'VpsSaasCMD:%s.%s'

def cmd_new(cmd, host_id , id):
    if not host_id:
        return
    if not cmd:
        return
    key = REDIS_VPS_SAAS_CMD%(host_id, cmd)
    p = redis.pipeline()

    if cmd == CMD.REBOOT:
        p.zadd(key, time(), id)
    else:
        p.lrem(key, 0 , id)
        p.rpush(key, id)

    p.execute()


def cmd_get(host_id, cmd):
    key = REDIS_VPS_SAAS_CMD%(host_id, cmd)
    #print cmd == CMD.REBOOT
    if cmd == CMD.REBOOT:
        now = time()
        redis.zremrangebyscore(key, 0 , now-600) #存活期 10 分钟
        t = redis.zrange(key, 0, 0)
        if t:
            t = t[0]
            redis.zadd(key, t, now)
            return int(t)
    else:
        t = redis.rpoplpush(key , key)
        #print "t", t
        if t:
            return int(t)
    #print "cmd", ".............."
    return 0

def cmd_done(host_id, cmd, id, state, message):
    if cmd:
        key = REDIS_VPS_SAAS_CMD%(host_id, cmd)
        if cmd == CMD.REBOOT:
            count = redis.zrem(key, id)
        else:
            count = redis.lrem(key, 0, id)
    else:
        count = 0
    return count

if __name__ == '__main__':
#    cmd = cmd_by_host_id(2)
#    print cmd_done(2, cmd)
#    print cmd
    #from time import time
    #cmd_done(1,CMD.OPEN, 36,0,"")
    #vps_saas_cmd_reboot(3, 95)
    pass
     
    
    
