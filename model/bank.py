#coding:utf-8
import _env
from model._db import Model , ModelMc, redis, redis_key, McLimitA, McNum
from misc.lib.base.timetool import today_days

#CREATE TABLE `work_vps`.`VmBank` (
#  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
#  `cent` BIGINT UNSIGNED NOT NULL,
#  `update` BIGINT UNSIGNED NOT NULL,
#  PRIMARY KEY (`id`)
#)
#ENGINE = MyISAM;

class VmBank(Model):
    pass

class VmCost(Model):
    @property
    def price(self):
        return (self.end-self.begin)*self.cent/100.0

def vm_bank_get(vm_id):
    vm_bank_refresh(vm_id)
    return VmBank.get(vm_id).cent

def vm_cost_list(vm_id):
    return list(VmCost.where(vm_id=vm_id).order_by("end desc"))

def vm_bank_refresh(vm_id):
    today = today_days()
    bank = VmBank.get(vm_id)
    if not bank:
        bank = VmBank(id=vm_id, update=today, cent=0)
        bank.save()
    if bank.update == today:
        return bank
    #        update                   today 
    # begin                end
    diff = 0
    update = bank.update

    for i in VmCost.where(vm_id=vm_id).where('begin<%s', today).where('end>%s', update): 
        if i.end < today:
            diff += (i.end - update)*i.cent
        else:
            diff += (today - update)*i.cent

    bank.cent -= diff
    bank.update = today
    bank.save()
    return bank

def vm_cost_new(vm_id, price, days, begin=None):
    cent = int(price * 100 / days)
    if begin is None:
        begin = today_days()

    bill = VmCost(vm_id=vm_id, begin=begin, end=begin+days, cent=cent)
    bill.save()

    bank = VmBank.get_or_create(id=vm_id)
    bank.update=begin
    if bank.cent is None:
        bank.cent = 0
    bank.cent+=cent*days
    bank.save()
   
    vm_bank_refresh(vm_id)


if __name__ == '__main__':
    pass
   # existed = set() 
   # for i in VmCost.where():
   #     if i.vm_id not in existed:
   #         existed.add(i.vm_id)
   #         cost_list = list(VmCost.where(vm_id=i.vm_id))
   #         if len(cost_list)>1:
   #             begin_end = set()
   #             for j in cost_list:
   #                 t = (j.begin, j.end)
   #                 if t not in begin_end:
   #                     begin_end.add(t)
   #                     continue
   #                 print begin_end, t , t not in begin_end
   #                 print i.vm_id, j.id, i.begin, i.end
   #                 j.delete()
   # 
   # existed = set() 
   # for i in VmCost.where():
   #     if i.vm_id not in existed:
   #         existed.add(i.vm_id)
   #         cost_list = vm_cost_list(i.vm_id)
   #         count = 0
   #         for j in cost_list:
   #             count += j.cent*(j.end - j.begin)
   #             print count
   #         b = VmBank.get(i.vm_id)
   #         b.cent = count
   #         b.update = i.begin
   #         b.save()
