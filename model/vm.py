#coding:utf-8
import _env
from model._db import Model , ModelMc, redis, redis_key, McLimitA, McNum
from misc.lib.base.password import password_random
from model.vps.host import host_allocate_template, vps_host_bind_vm, vps_host_rm_vm
from model.vps.template import VmTemplate
from zweb.attrcache import attrcache
from model.base.user import User
from model.vps.const import VM_STATE, VM_STATE_CN
from model.vps.vm_harddisk import vm_harddisk_new, vm_harddisk_new_default, vm_harddisk_delete, vm_harddisk_upgrade
from model.base.user_mail import user_id_by_mail, mail_by_user_id
from model.vps.saas.cmd import cmd_new, CMD
from model.vps.datacenter import VpsDataCenter
from misc.lib.base.timetool import today_days
from misc.config import HOST
from model.vps.ip import VpsIp, ip2int, vps_ip_autobind, vps_ip_unbind 
from model.vps.ip_inner import VpsIpInner, vps_ip_inner_autobind, vps_ip_inner_unbind

#REDIS_VPS_IP_TYPE = redis_key.VpsIpType("%s")

class Vm(ModelMc):
    @attrcache
    def user(self):
        return User(self.user_id)

    def can_admin(self, user_id):
        return self.user_id == int(user_id)

    @attrcache
    def template(self):
        return VmTemplate.mc_get(self.vm_template_id)

    @property
    def state(self):
        return self.state_

    @state.setter
    def state(self, value):
        _old_state = self.state_
        value = int(value)
        if _old_state == value:
            return
        vm_id = self.id
        self.state_ = value
        if value == VM_STATE.PAY:
            if _old_state == VM_STATE.RESERVE or not _old_state:
                from model.vps.bank import vm_cost_new
                day = self.date_end
                vm_cost_new(vm_id, self.template.price(day), day)
                self.date_end = today_days()+self.date_end
                if not self.host_id:
                    template = self.template
                    host = host_allocate_template(self.data_center_id, template)
                    if host:
                        self.host_id = host.id
                if self.host_id:
                    vps_host_bind_vm(self.host_id, self)
                vps_ip_inner_autobind(vm_id, self.data_center_id)
                vps_ip_autobind(vm_id, self.data_center_id)
                vm_open(vm_id)
                return
            elif _old_state == VM_STATE.CLOSE:
                vm_open(vm_id)
                return
        if _old_state == VM_STATE.CLOSE and value == VM_STATE.OPEN:
            vm_open(vm_id)
            return
        if _old_state == VM_STATE.PAY and value == VM_STATE.OPEN:
            from model.vps.vm_pay import vm_pay_open
            vm_pay_open(vm_id)
        if _old_state == VM_STATE.OPEN and value == VM_STATE.CLOSE:
            vm_close(self)
            return
        if value == VM_STATE.RM:
            if _old_state == VM_STATE.CLOSE or _old_state == VM_STATE.PAY:
                # vm_delete (vm_id)
                return
            elif _old_state == VM_STATE.RESERVE:
                return

#raise Exception ("invalid state change vm_id=%s old_state=%s new_state=%s" % (vm_id, vm.state, new_state))

def vm_list_by_user_id(user_id):
    return Vm.where(user_id=user_id).order_by('id desc').where('state_>=%s', VM_STATE.PAY)

def vm_new(user_id, vm_template_id, os_id, data_center_id, date_end, host_id=None, ip_type=None, password=None, id=None, ):
    if password is None:
        password = password_random()
    template = VmTemplate.get (vm_template_id)
    if not template:
        raise Exception ('vm template_id=%s not found' % (vm_template_id))
    if not id:
        vm = Vm()
    else:
        vm = Vm.get_or_create(id=id)
    datacenter = VpsDataCenter.get(data_center_id)
    os_template_id = os_id
    vm.user_id = user_id
    vm.date_end = date_end
    vm.country_id = datacenter.country_id
    vm.host_id = host_id
    vm.vm_template_id = vm_template_id
    vm.os_id = os_id
    vm.os_template_id = os_template_id
    vm.data_center_id = data_center_id
    vm.state_ = VM_STATE.RESERVE
    vm.cpu = template.cpu
    vm.ram = template.ram
    vm.hd_total = template.hd
    vm.bandwidth = template.bandwidth
    vm.netflow_quota = template.netflow
    vm.password = password
    vm.save()
    vm_harddisk_new_default (vm.id, vm.hd_total)
    return vm

def vm_id_li(limit, offset, state, host_id=None):
    if host_id:
        return Vm.where(state_=state, host_id=host_id).order_by('id desc').column('id', limit, offset)
    else:
        return Vm.where(state_=state).order_by('id desc').column('id', limit, offset)

def vm_list(limit, offset, state, host_id=None):
    vm_li = Vm.mc_get_list(vm_id_li(limit, offset, state, host_id))
    return vm_li

def vm_state_count (host_id=None):
    counts = dict ()
    for k in VM_STATE_CN.iterkeys ():
        if host_id:
            counts[k] = Vm.where(state_=k, host_id=host_id).count ()
        else:
            counts[k] = Vm.where(state_=k).count ()
    return counts

def vm_upgrade_template (vm_id, vm_template_id, send_cmd=False):
    template = VmTemplate.get (vm_template_id)
    if not template:
        raise Exception ('vm template_id=%s not found' % (vm_template_id))
    vm = Vm.get (vm_id)
    if not vm:
        return
    vm.ram = template.ram
    if template.hd < vm.hd_total:
        raise Exception ('cannon upgrade vm hardisk')
    vm.hd_total = template.hd
    vm.bandwidth = template.bandwidth
    vm.netflow_quota = template.netflow
    vm.vm_template_id = vm_template_id
    vm.save ()
    vm_harddisk_upgrade (vm_id, vm.hd_total)
    if send_cmd:
        vm_upgrade (vm_id)

def vm_close (vm):
    if vm and vm.host_id:
        cmd_new(CMD.CLOSE, vm.host_id, vm.id)

def vm_open (vm_id):
    vm = Vm.get (vm_id)
    if vm and vm.host_id:
        cmd_new(CMD.OPEN, vm.host_id, vm.id)

def vm_upgrade (vm_id):
    vm = Vm.get (vm_id)
    if vm and vm.host_id:
        cmd_new(CMD.UPGRADE, vm.host_id, vm.id)


def vm_delete (vm_id):
    vm = Vm.get (vm_id)
    if not vm or (vm.state != VM_STATE.PAY and vm.state != VM_STATE.CLOSE):
        return
    if vm.host_id:
        cmd_new(CMD.OPEN, host_id, vm.id)

def vm_delete_done (vm_id, host_id):
    vm = Vm.get (vm_id)
    if not vm or vm.host_id != host_id or vm.state != VM_STATE.RM:
        return
    vps_ip_unbind (vm_id)
    vps_ip_inner_unbind (vm_id)
    vm_harddisk_delete (vm_id)
    vps_host_rm_vm  (vm)

def vm_reinstall (vm_id):
    vm = Vm.mc_get (vm_id)
    if not vm:
        return
    cmd_new(CMD.OS, vm.host_id, vm.id)

def vm_reboot (vm_id):
    vm = Vm.mc_get (vm_id)
    if not vm:
        return
    cmd_new(CMD.REBOOT, vm.host_id, vm.id)

def vm_search(query):
    li = []
    if '@' in query:
        user_id = user_id_by_mail(query)
        li = vm_list_by_user_id(user_id)
    elif query.isdigit():
        li = Vm.where(id=query)
    elif '.' in query:
        ip = VpsIp.get(ip=ip2int(query))
        if ip and ip.vm_id:
            li = Vm.where(id=ip.vm_id)
        else:
            ip = VpsIpInner.get(ip=ip2int(query))
            if ip and ip.vm_id:
                li = Vm.where(id=ip.vm_id)
    return li

def vm_transfer(vm_id, to_user_id):
    vm = Vm.get(vm_id)
    vm.user_id = to_user_id
    vm.save()




if __name__ == '__main__':
    #vm_upgrade_template (331, 21)
#    vps_ip_unbind(562)
    pass

    #vm_transfer(616, int(to_user_id))

    #vm = Vm.get(309)
    #print vm.user_id 
#    vm_reboot(294)   

    #vm = Vm.get(517)
    #vm.data_center_id = 2
    #vm.save()

