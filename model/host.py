#!/usr/bin/env python
#coding:utf-8


import _env
from model._db import ModelMc, Model, redis
from model.vps.const import *
from model.vps.datacenter import *
from collections import OrderedDict
from misc.lib.vps.ip import int2ip, ip2int

REDIS_VPS_HOST_COUNT = 'VpsHostCount' #记录data_center的机器数目



#CREATE TABLE IF NOT EXISTS `VpsHost` (
#  `id` bigint(20) NOT NULL,
#  `ip_ext` int(11) NOT NULL,
#  `ip_inner` int(11) DEFAULT NULL,
#  `data_center_id` bigint(11) NOT NULL,
#  `ram_count` int(11) NOT NULL COMMENT 'unit M',
#  `ram_remain` int(11) NOT NULL DEFAULT '0' COMMENT 'unit M',
#  `hd_count` int(11) NOT NULL COMMENT 'unit G',
#  `hd_remain` int(11) NOT NULL DEFAULT '0' COMMENT 'unit G',
#  `vps_count` int(11) NOT NULL DEFAULT '0',
#  `vps_limit` int(11) NOT NULL,
#  `host_type` int(11) NOT NULL COMMENT 'vps environment type',
#  `alloc_bitmap` int(11) DEFAULT '0',
#  `txt` text,
#  PRIMARY KEY (`id`)
#) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='物理机';

class VpsHost(Model):
    @property
    def ram_used(self):
        res = self.ram_count - self.ram_remain
        return res > 0 and res or 0

    @property
    def hd_used(self):
        res = self.hd_count - self.hd_remain
        return res > 0 and res or 0

    @property
    def ip_ext_str (self):
        return self.ip_ext is not None and int2ip (self.ip_ext) or ''

    @property
    def ip_inner_str (self):
        return self.ip_inner is not None and int2ip (self.ip_inner) or ''



def vps_host_new(host_id, ip_ext_str, ip_inner_str, data_center_id, ram_count, hd_count, host_type):
    if host_type == 'centos6_xen':
        vps_limit = 39
    else:
        vps_limit = 0
    host = VpsHost(
        id=int(host_id),
        ip_ext=ip2int (ip_ext_str),
        ip_inner=ip2int (ip_inner_str),
        data_center_id=int(data_center_id),
        ram_count=int(ram_count),
        ram_remain=int(ram_count),
        hd_count=int(hd_count),
        hd_remain=int(hd_count),
        vps_count=0,
        vps_limit=int(vps_limit),
        host_type=VPS_HOST_TYPE_NAME2ID[host_type],
    )
    host.save()
    vps_host_count_reset (data_center_id)
    return host.id

def vps_host_count_reset (data_center_id):
    redis.hset(REDIS_VPS_HOST_COUNT, data_center_id, VpsHost.where (data_center_id=data_center_id).count ())

def vps_host_edit (host_id, ip_ext_str, ip_inner_str, data_center_id, ram_count, hd_count, host_type, vps_limit):
    host = VpsHost.get (host_id)
    if not host:
        raise Exception ('host not found')
    host.ip_ext = ip2int (ip_ext_str)
    host.ip_inner = ip2int (ip_inner_str)
    old_data_center_id = host.data_center_id

    host.data_center_id = data_center_id
    host.ram_count = ram_count
    host.hd_count = hd_count
    host.host_type = VPS_HOST_TYPE_NAME2ID[host_type]
    host.vps_limit = vps_limit
    host.save ()
    vps_host_count_reset (data_center_id)

    if old_data_center_id != data_center_id:
        vps_host_count_reset (old_data_center_id)
    return host

import time
def vps_host_count():
    print time.time (), redis.hgetall(REDIS_VPS_HOST_COUNT)

    return redis.hgetall(REDIS_VPS_HOST_COUNT)

def vps_host_list(data_center_id=None):
    if data_center_id:
        return VpsHost.where(data_center_id=data_center_id)
    else:
        return VpsHost.where()

def vps_host_update (host_id, hd_remain, ram_remain, hd_total=None, ram_total=None):
    host = VpsHost.get (host_id)
    if not host:
        return
    print hd_remain, ram_remain, hd_total, ram_total
    if ram_remain >= 0:
        host.ram_remain = ram_remain
    if hd_remain >= 0:
        host.hd_remain = hd_remain
    if ram_total >= 0:
        host.ram_count = ram_total
    if hd_total >= 0:
        host.hd_count = hd_total
    host.save ()

def vps_host_mv(vm, host_id):
    vps_host_rm(vm)


    host = VpsHost.get(host_id)

    if host:
        vm.host_id = host_id
        vm.save()

        host.vps_count += 1
        host.ram_remain -= vm.ram
        host.hd_remain -= vm.hd
        host.save()

def vps_host_bind_vm(host_id, vm):
    """ when unbind, host_id is None; 
        when in US host_id == 0. """
    host_new = None
    if host_id > 0:
        host_new = VpsHost.get(host_id)
        if not host_new:
            raise Exception ('host_id=%s not found' % (host_id))
    if vm.host_id and vm.host_id != host_id:
        host_old = VpsHost.get(vm.host_id)
        if host_old:
            host_old.vps_count -= 1
            host_old.ram_remain += vm.ram
            host_old.hd_remain += vm.hd_total
            host_old.save()
    if host_new:
        host_new.vps_count += 1
        host_new.ram_remain -= vm.ram
        host_new.hd_remain -= vm.hd_total
        host_new.save()
        vm.data_center_id = host_new.data_center_id
    vm.host_id = host_id
    vm.save ()


def vps_host_rm_vm(vm):
    vps_host_bind_vm (None, vm)

# alloc_bitmap == 0 : will not auto alloc
# alloc_bitmap == None: can auto allocate all vps_template

def _find_host_with_space (data_center_id, ram, hd):
    host_list = VpsHost.where ('alloc_bitmap is null and data_center_id=%s and (vps_limit=0 or vps_count<vps_limit)' % (data_center_id)).\
        where ('ram_remain>%d and hd_remain >%d' % (ram, hd)).order_by ('ram_count ASC, ram_remain DESC')
    if len(host_list) > 0:
        return host_list[0]
    return None

def _find_host_fitting_with_space (data_center_id, prefer_ram, prefer_hd, ram, hd):
    host_list = VpsHost.where ('alloc_bitmap is null and data_center_id=%s and (vps_limit=0 or vps_count<vps_limit)' % (data_center_id)).\
        where ('ram_remain>%d and hd_remain >%d' % (ram, hd)).\
        where ('ram_count>=%d and hd_count>=%d' % (prefer_ram, prefer_hd)).\
order_by ('ram_count ASC, ram_remain DESC')
    if len(host_list) > 0:
        return host_list[0]
    return None

def _host_allocate (data_center_id, ram, hd):
    host = None
    if not host and ram >= 4096:
        host = _find_host_fitting_with_space (data_center_id, prefer_ram=64000, prefer_hd=5000,
                ram=ram * 1.1, hd=int(hd * 2))
    if not host and ram >= 2048:
        host = _find_host_fitting_with_space (data_center_id, prefer_ram=48000, prefer_hd=3000,
                ram=ram * 1.2, hd=int(hd * 2))
    if not host and ram >= 1024:
        host = _find_host_fitting_with_space (data_center_id, prefer_ram=3200, prefer_hd=1600,
                ram=ram * 1.3, hd=int(hd * 2))
    if not host and ram >= 512:
        host = _find_host_fitting_with_space (data_center_id, prefer_ram=2400, prefer_hd=1600,
                ram=ram * 1.5, hd=int(hd * 2))
    if not host:
        host = _find_host_with_space (data_center_id, ram=ram + 512, hd=int(hd * 2))
    return host

def host_allocate_template (data_center_id, template):
    return _host_allocate (data_center_id, template.ram, template.hd)

if __name__ == '__main__':
    pass
    #host = VpsHost.get (2)
    #from model.vps.vm import Vm, VM_STATE
    #host.alloc_bitmap = 0
    #host.save ()
    #for i in VpsHost.where():
    #    t = Vm.where(host_id=i.id).where('state_>=%s', VM_STATE.OPEN)
    #    t = t.count()
    #    i.vps_count = t
    #    i.save()
    #    print t
    
#    h = _host_allocate (2, 4096, 200)
#    if h:  print h.id, h.ram_count, h.ram_remain, h.hd_count, h.hd_remain
#    else: print "orz"
#    h = _host_allocate (2, 2048, 60)
#    if h:  print h.id, h.ram_count, h.ram_remain, h.hd_count, h.hd_remain
#    else: print "orz"
#    h = _host_allocate (2, 1024, 30)
#    if h:  print h.id, h.ram_count, h.ram_remain, h.hd_count, h.hd_remain
#    else: print "orz"
#    h = _host_allocate (2, 512, 15)
#    if h:  print h.id, h.ram_count, h.ram_remain, h.hd_count, h.hd_remain
#    else: print "orz"
#    

#    vps_host_list ()
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 :
