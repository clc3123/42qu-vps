#coding:utf-8
import _env
from misc.lib.base.timetool import today_days
from model.base.gid import CID
from model.vps.vm import VM_STATE, Vm
from model.vps.template import VmTemplate 
from model.base.sendmail_srv import render_mail
from model.vps.ip import vm_ip_list 
from model.base.user_mail import mail_by_user_id
from model.vps.vm_os import VPS_OS_DICT 
from model.vps.datacenter import VpsDataCenter
from model.pay.const import PAY_ORDER_RECALL, PAY_GOODS_CN, Goods, CID_GOODS
from model.vps.bank import vm_cost_list, vm_cost_new
MAIL_GOOGLE_GROUP = "42qu-vps-order@googlegroups.com"

def vm_payed(vm_id):
    vm = Vm.mc_get(vm_id)
    if not vm:
        return
    vm.state = VM_STATE.PAY
    vm.save()
    datacenter = VpsDataCenter.mc_get (vm.data_center_id)
    render_mail(
        '/vps/_mail/payed.html',
        'vps%s 待开通 ; 机房 : %s'%(
            vm_id, 
            datacenter.name
        ),
        MAIL_GOOGLE_GROUP,
        0,
        id = vm_id,
    )

def vm_renew(vm_id, days):
    vm = Vm.mc_get(vm_id)
    
    cost_list = vm_cost_list(vm_id)
    today = today_days()
    begin = today
 
    if cost_list:
        end = cost_list[0].end
        if end > today:
            begin = end

    vm_cost_new(vm_id, vm.template.price(days), days, begin)  
 
    if vm.state == VM_STATE.CLOSE:
        vm.state = VM_STATE.OPEN
    vm.date_end = begin+days 
    vm.save() 

PAY_ORDER_RECALL[CID.VPS] = lambda order:vm_payed(order.rid)
PAY_ORDER_RECALL[CID.VPS_VM_RENEW] = lambda order:vm_renew(order.rid, order.address_id)

def vm_pay_open(vm_id):
    vm = Vm.mc_get(vm_id)
    if not vm:
        return
    user_id = vm.user_id
    for mail in (MAIL_GOOGLE_GROUP, mail_by_user_id(user_id)):
        render_mail(
            '/vps/_mail/open.html',
            'vps%s 已开通 ; 系统 : %s'%(vm_id, VPS_OS_DICT[vm.os_id]),
            mail,
            user_id,
            id = vm_id,
            password = vm.password,
            ip_list = vm_ip_list(vm_id)
        )

class GoodsVm(Goods):
    COLUMN= ("id","cpu","ram","hd","cent")

    @property
    def column(self): 
        vm = Vm.mc_get(self.id)
        #print self.id
        template = VmTemplate.mc_get(vm.vm_template_id)
        return [getattr(template,i) for i in self.COLUMN]

class GoodsVmRenew(GoodsVm):
    pass
CID_GOODS[CID.VPS] = GoodsVm 
CID_GOODS[CID.VPS_VM_RENEW] = GoodsVmRenew 
PAY_GOODS_CN[CID.VPS] = "VPS - 新开"
PAY_GOODS_CN[CID.VPS_VM_RENEW] = "VPS - 续费"

if __name__ == "__main__":
    vm_pay_open(1)
