#!/usr/bin/env python
# coding:utf-8

import _env


from model._db import ModelMc, Model, redis
from zorm.mc import McCache, McCacheA, McNum

from model.vps.datacenter import VpsDataCenter, vps_datacenter_list
from misc.lib.vps.ip import int2ip, ip2int
from misc.lib.vps.mac import randomMAC


REDIS_VPS_IP_TOTAL = 'VpsIpTotal'
REDIS_VPS_IP_USED = 'VpsIpUsed'




class VpsIp(Model):
    @property
    def ip_str(self):
        return int2ip(self.ip)

    @property
    def netmask_str(self):
        return int2ip(self.netmask)

    @property
    def gateway_str(self):
        return int2ip(self.gateway)

mc_vm_ip_list = McCacheA('VmIpList:%s')

@mc_vm_ip_list('{vm_id}')
def _vm_ip_list(vm_id):
    return VpsIp.where(vm_id=vm_id).column('ip')

def vm_ip_list(vm_id):
    return map(int2ip, _vm_ip_list(vm_id))

def _mc_flush(vm_id):
    mc_vm_ip_list.delete(vm_id)

def vps_ip_unbind(vm_id, ip=None):
    if ip:
        ips = VpsIp.where(vm_id=vm_id, ip=ip)
    else:
        ips = VpsIp.where(vm_id=vm_id)
    for ip in ips:
        if ip.vm_id and ip.vm_id == vm_id:
            redis.hincrby(REDIS_VPS_IP_USED, ip.data_center_id, -1)
            ip.vm_id = 0
            ip.save()
    _mc_flush(vm_id)
  

def vps_ip_bind(vm_id, ip):
    ip = VpsIp.get(ip=ip)
    if not ip:
        return False
    if not ip.vm_id:
        redis.hincrby(REDIS_VPS_IP_USED, ip.data_center_id, 1)
        ip.vm_id = vm_id
    elif ip.vm_id != vm_id:
        raise Exception ('ip %s already belong to vm_id=%s' % (int2ip(ip.ip), vm_id))
    ip.save()
    _mc_flush(vm_id)
    return ip

def vps_mac_new():
    while True:
        _mac = randomMAC()
        if not VpsIp.get(mac=_mac):
            return _mac


#def vps_mac_autobind(vm_id):
#    ip = VpsIp.get(vm_id=vm_id)
#    ip.mac = mac()
#    ip.save()

def vps_ip_new(data_center_id, vm_id, gateway, netmask, begin, end=None):
    gateway = int(gateway)
    netmask = int(netmask)
    if not vm_id:
        vm_id = 0
    total = 0
    if end is None:
        end = begin
    for ip in xrange(begin, end+1):
        if ip in (gateway, netmask):
            continue
        vps = VpsIp.get_or_create(ip=ip)
        if not vps.id:
            total += 1
        vps.netmask = netmask
        vps.gateway = gateway
        vps.mac = vps_mac_new()
        vps.data_center_id = data_center_id
        vps.vm_id = vm_id
        vps.save()
    redis.hincrby(REDIS_VPS_IP_TOTAL, data_center_id, total)
    return total

def vps_ip_used_total():
    return redis.hgetall(REDIS_VPS_IP_USED), redis.hgetall(REDIS_VPS_IP_TOTAL)

def vps_ip_rm(id):
    """ cannot delete using ip """
    ip = VpsIp.get(id)
    if ip and not ip.vm_id:
        data_center_id = ip.data_center_id
        redis.hincrby(REDIS_VPS_IP_TOTAL, data_center_id, -1)
        ip.delete()
        return True
    else:
        return False

def vps_ip_count(data_center_id=0):
    if data_center_id:
        return VpsIp.where (data_center_id=data_center_id).count ()
    else:
        return VpsIp.where ().count ()

def vps_ip_list(data_center_id, limit, offset):
    return VpsIp.where(data_center_id=data_center_id).order_by('ip')[offset:limit+offset]

def vps_ip_autobind(vm_id, data_center_id):
    #clear binded ip not in the same datacenter
    ips = VpsIp.where(vm_id=vm_id)
    if ips:
        for ip in ips:
            if ip.data_center_id != data_center_id:
                vps_ip_unbind (vm_id, ip.ip)
    ips = VpsIp.where(vm_id=vm_id, data_center_id=data_center_id)
    if not ips:
        ip = VpsIp.get(data_center_id=data_center_id, vm_id=0)
        if ip:
            vps_ip_bind (vm_id, ip.ip)
        else:
            ip = VpsIp.get(data_center_id=data_center_id, vm_id=None)
            if ip:
                vps_ip_bind (vm_id, ip.ip)
        return ip

def vps_ip_count_reset():
    datacenters = vps_datacenter_list ()
    for datacenter in datacenters:
        i = datacenter.id
        redis.hset(REDIS_VPS_IP_TOTAL, i, VpsIp.where(data_center_id=i).count())
        redis.hset(REDIS_VPS_IP_USED, i, VpsIp.where('vm_id!=0').where(data_center_id=i).count())




if __name__ == '__main__':
    pass
    #ip = VpsIp.get (ip=ip2int("218.245.3.189"))
    #if ip:
    #    ip.mac = "00:16:3e:40:b0:69"
    #    ip.save ()
    #vps_ip_count_reset ()
#expandtab shiftwidth=4 softtabstop=4 :
