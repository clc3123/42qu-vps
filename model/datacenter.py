#!/usr/bin/env python
# coding:utf-8

import _env
from model._db import ModelMc, Model, redis
from model.vps.const import *

class VpsCountry (ModelMc):
    pass

class VpsDataCenter(ModelMc):
    pass

VPS_COUNTRY_CODE2ID = dict ()
VPS_COUNTRY_ID2NAME = dict ()
#VPS_DATA_CENTER_COUNTRY = dict()
#
for country in VpsCountry.where():
    VPS_COUNTRY_CODE2ID[country.code] = country.id
    VPS_COUNTRY_ID2NAME[country.id] = country.name
#    VPS_DATA_CENTER_COUNTRY[country.id] = VpsDataCenter.where(country_id=country.id).order_by("id desc")
 

#VPS_DATA_CENTER = VpsDataCenter.where()
#VPS_DATA_CENTER_DICT = dict((i.id, i) for i in VPS_DATA_CENTER)

def vps_datacenter_dict ():
    datacenters = vps_datacenter_list ()
    return dict ((i.id, i) for i in datacenters)
        

def vps_datacenter_list (country_id=None):
    if country_id:
        return VpsDataCenter.where (country_id=country_id)
    else:
        return VpsDataCenter.where ()

if __name__ == '__main__':
    pass
    #print VPS_DATA_CENTER_COUNTRY
    for datacenter in vps_datacenter_list ():
        print datacenter.id, datacenter.name, datacenter.is_full, datacenter.country_id
    
