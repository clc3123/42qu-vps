#coding:utf-8
import _env
from model._db import Model, ModelMc, redis, redis_key
from time import time
from binascii import a2b_hex
from msgpack import dumps, loads
from model.pay.bank import bank_transfer


ONE_DAY = 60*60*24
ONE_YEAR = ONE_DAY*365

REDIS_VPS_REWARD_BY = 'rewardby%s'
EXPIRE = ONE_DAY*15

class VpsRewardPay(Model):
    pass

def reward_by(user_id, xsrf, by, referer):
    user_id = int(user_id)
    by = int(by)
    if user_id == by or not by:
        return
    xsrf = a2b_hex(xsrf)
    for key in ( user_id , xsrf ):
        if not key:
            continue
        redis.setex(REDIS_VPS_REWARD_BY%key, EXPIRE, dumps([by, referer]))



def reward_check(user_id, xsrf):
    xsrf = a2b_hex(xsrf)
    by_and_referer = redis.get(REDIS_VPS_REWARD_BY%user_id) or redis.get(REDIS_VPS_REWARD_BY%xsrf)
    if by_and_referer:
        return loads(by_and_referer)

def vps_reword_by_user_id(user_id):
    bill_list = VpsRewardPay.where(by=user_id)
    return bill_list

def vps_reward_pay(user_id, trade_id, cent, xsrf):
    now = int(time())
    reward = VpsRewardPay.get(user_id)
    if reward:
        if now - reward.begin_time > ONE_YEAR:
            return
        reward.cost += cent
    else:
        by_and_referer = reward_check(user_id, xsrf)
        if not by_and_referer:
            return
        else:
            by, referer = by_and_referer
            by = int(by)
            if user_id == by:
                if user_id != 10050704:
                    return
                else:
                    # test
                    cent = cent * 100
            reward = VpsRewardPay(
                id=user_id, 
                by=by, 
                referer=referer, 
                cost=cent,
                begin_time=now
            )

    if reward:
        reward.update_time=now
        if reward.by:
            if   reward.cost >= 200000:
                back = 20000
            elif reward.cost >= 100000:
                back = 10000
            else:
                back = 0

            if back:
                if not reward.reward:
                    reward.reward = 0
                diff = back - reward.reward
                if diff > 0:
                    reward.reward = back
                    #bank_transfer(0, reward.by, diff)
     
        reward.save()

if __name__ == '__main__' :
#    print ONE_YEAR
#    print vps_reward_pay(1000, 123456)
    #reward_check(0, '29920e5108614d09b70a24bf468804d7')
    #print vps_reward_pay(3, 0, 100000, '29920e5108614d09b70a24bf468804d7')
    import time
    for r in VpsRewardPay.where():
        print r.referer, time.strftime ("%y-%m-%d", time.localtime (r.begin_time)), r.by, r.reward

