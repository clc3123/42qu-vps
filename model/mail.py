#coding:utf-8
import _env
import model._db
from model._db import redis, redis_key
from model.base.user_mail import mail_by_user_id
from model.vps.vm import Vm
from model.vps.const import VM_STATE, REMIND_MAIL_STATE
from model.vps.datacenter import VPS_COUNTRY_CODE2ID
from model.base.user import User
from model.base.gid import CID
from misc.config import HOST
from zweb.signal import SIGNAL
from model.base.mail_verify  import mail_verify_mail, redis_mail_new, redis_mail_rm, redis_mail_list, redis_mail_verifyed 

def get_active_user_mail (host_id=None, data_center_id=None):
    mail_dict = dict ()
    vm_list = None
    if host_id:
        vm_list = Vm.where (country_id=VPS_COUNTRY_CODE2ID['cn'], state_=VM_STATE.OPEN, host_id=host_id)
    elif data_center_id:
        vm_list = Vm.where (country_id=VPS_COUNTRY_CODE2ID['cn'], state_=VM_STATE.OPEN, data_center_id=data_center_id)
    else:
        vm_list = Vm.where (country_id=VPS_COUNTRY_CODE2ID['cn'], state_=VM_STATE.OPEN)
    for vm in vm_list:
        mail_dict[vm.id] = mail_by_user_id (vm.user_id)
    return mail_dict


REDIS_VPS_REMIND_MAIL = redis_key.RemindMail('%s')

def remind_mail_new_mail(vm_id, mail):

    mail_id = mail_verify_mail(
        '/vps/_mail/remind_mail_verify.html',
        '%s VPS添加提醒邮箱验证'%HOST,
        mail,
        'http://%s/auth/mail/'+str(CID.VPS)+'/verify/'+str(vm_id)+'/%s/%s',
        vm_id
    )
    return mail_id

def remind_mail_new(vm_id, mail):
    mail_id = remind_mail_new_mail(vm_id, mail)
    redis_mail_new(mail_id, REDIS_VPS_REMIND_MAIL, vm_id)

def remind_mail_renew(vm_id, mail):
    remind_mail_new_mail(vm_id, mail)

@SIGNAL.mail_verify
def remind_mail_verifyed(cid, vm_id, mail_id):
    if int(cid) == CID.VPS:
        redis_mail_verifyed(mail_id, REDIS_VPS_REMIND_MAIL, vm_id)

def remind_mail_del(vm_id, mail_id):
    redis_mail_rm(mail_id, REDIS_VPS_REMIND_MAIL, vm_id)

def remind_mail_list(vm_id):
    mail_list = redis_mail_list(REDIS_VPS_REMIND_MAIL, vm_id)  
    return mail_list


if __name__ == '__main__':
    #import pprint
    #mail_dict = get_active_user_mail (8)
    #pprint.pprint (mail_dict)
    pass
