#coding:utf-8

import _env
from model._db import Model , ModelMc, redis, redis_key
from model.base.gid import CID, gid
from model.vps.const import Bill, CID_BILL, VM_STATE
from model.pay.const import PAY_GOODS_CN
from model.vps.datacenter import VpsCountry, VpsDataCenter
from model.vps.template import VmTemplate 
from model.vps.vm import Vm

class Bills(ModelMc):
    pass

def vm_bill_day_new(country, data_center, template, cid, volume, cent, days):
    bill = Bills()
    bill.country_id = country 
    bill.data_center_id = data_center 
    bill.template_id = template
    bill.cid = cid
    bill.volume = volume 
    bill.cent = cent 
    bill.days= days 
    bill.save()

#TODO  �����������
#def vm_bill_list_detail(country, datacenter, template, days):
#    bill_ids = Bills.where(country_id=cid, data_center_id=datacenter, template_id=template, days=days)
#   id  country_id  template_id     data_center_id 
#    pass


def vm_bill_list_expiring(days):
    bill_list = Vm.where('date_end <= %s'%days, state_=VM_STATE.OPEN)
    volume = 0
    cent = 0
    for vm in bill_list:
        cost = VmTemplate.where(id=vm.vm_template_id).column('cent')
        volume += 1
        for i in cost:
            cent += i * 30 
    return [volume, cent]

def bill_list_cid(cid):
    bill_list = BillMonth.where(cid=cid).order_by('id desc')
    return bill_list

def bill_list_by_month(day):
    bill_list = BillMonth.where(days=day).order_by('id desc')
    return bill_list

class BillCountry(Bill):
    @property
    def column(self): 
        country = VpsCountry.mc_get(self.id)
        return country.name

class BillDataCenter(Bill):

    @property
    def column(self): 
        data_center= VpsDataCenter.mc_get(self.id)
        return data_center.name

class BillTemplate(Bill):

    @property
    def column(self): 
        template = VmTemplate.mc_get(self.id)
        if template.ram <= 512:
            ram = "%s M"%(template.ram) 
        else:
            ram = "%s G"%int(template.ram/1000)
        return [template.country,ram]

class BillGoodsCid(Bill):

    @property
    def column(self): 
        goods_cid = PAY_GOODS_CN[self.id] 
        return goods_cid 

CID_BILL[CID.BILL_COUNTRY] = BillCountry
CID_BILL[CID.BILL_DATA_CENTER] = BillDataCenter
CID_BILL[CID.BILL_TEMPLATE] = BillTemplate
CID_BILL[CID.BILL_GOODS_CID] = BillGoodsCid
    

class BillMonth(ModelMc):
    pass

def bill_month_new(cid, tid, cent, volume, month):

    bill_list = BillMonth.where(cid=cid, tid=tid, days=month)
    if bill_list:
        for bill in bill_list:
            bill.cid = cid 
            bill.tid = tid 
            bill.cent = cent 
            bill.volume = volume 
            bill.days = month 
            bill.save()
    else:
        bill = BillMonth()
        bill.cid = cid 
        bill.tid = tid 
        bill.cent = cent 
        bill.volume = volume 
        bill.days = month 
        bill.save()


if __name__ == '__main__':
    #bill_new(1, 2, 22, 3009, 22, 125000,15553 )
    pass
