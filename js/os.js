(function() {

  window.os = function(id) {
    return $.getJSON('/j/os', function(os_list) {
      var item, select, _, _i, _len;
      _ = $.html();
      _("<select>\n<option value=\"0\">-</option>");
      for (_i = 0, _len = os_list.length; _i < _len; _i++) {
        item = os_list[_i];
        _("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
      }
      _("</select>");
      select = _.html();
      return $.fancybox({
        content: "<div style=\"font-size: 16px;margin-top: 27px;\"><span style=\"color:red\">警告</span>:重装将清空除 /root 和 /home 外的所有数据，请做好备份</div>\n<div id=\"vps_os_select\">" + select + "</div>\n<div style=\"margin-top: 27px;margin-top: 27px;\">输入42区登录密码确认</div>\n<div style=\"margin-top: 27px;\"><input type=\"password\" name=\"password\"style=\"padding:14px 7px;border:1px solid #ccc;width:300px\" ></div>\n<span style=\"border: 1px solid #CCCCCC;display: inline-block;height: 42px;margin-top: 27px;\"><button class=\"reinstall BTN\" >vps " + id + " 重装系统 并重置密码</button></span>",
        afterShow: function() {
          return $('.reinstall').click(function() {
            var os_id, password;
            password = $('input[name="password"]').val();
            os_id = $('#vps_os_select select').val();
            return $.postJSON("/j/os/" + id, {
              "password": password,
              "os": os_id
            }, function(data) {
              if (data.msg) {
                alert(data.msg);
              } else {
                alert("正在重装系统，去喝杯咖啡吧！");
              }
              return $('.fancybox-close').trigger('click');
            });
          });
        }
      });
    });
  };

  $(function() {
    return $(".mail_new").click(function() {
      var mail;
      mail = prompt('请输入邮箱,并提醒该用户验证邮箱！');
      if (mail) {
        return $.postJSON("/admin/mail/new/" + this.rel + "/" + mail, function() {
          return location.reload();
        });
      }
    });
  });

  window.mail_del = function(vm_id, mail_id) {
    return $.postJSON("/admin/mail/rm/" + vm_id + "/" + mail_id, function() {
      alert('已删除');
      return location.reload();
    });
  };

  window.mail_renew = function(vm_id, mail) {
    return $.postJSON("/admin/mail/renew/" + vm_id + "/" + mail, function() {
      alert('已重新发送');
      return location.reload();
    });
  };

  window.vm_transfer = function(vm_id, query) {
    query = prompt('请输入转让给的用户邮箱');
    if (query) {
      return $.getJSON("/admin/" + vm_id + "/user/" + query, function(user_id) {
        if (user_id === 0 || !user_id) {
          return alert("该用户不存在！");
        } else {
          return $.postJSON("/admin/" + vm_id + "/transfer/" + user_id, function() {
            return location.reload();
          });
        }
      });
    }
  };

}).call(this);
