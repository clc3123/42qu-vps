(function() {

  $(function() {
    var cost, day_cn, day_us, err, price, re, type;
    day_cn = $('input[name="days"]');
    price = $('.price');
    err = $('.day_err');
    type = $('#template').val();
    day_us = $('select[name="days"]');
    re = /^[0-9]+$/;
    day_cn.focus();
    cost = function(day) {
      var days;
      days = $.trim(day.val());
      price.html('');
      price.addClass("loading");
      if (re.test(days)) {
        err.html('');
        return $.get("/j/price/" + type + "/" + days, function(data) {
          price.removeClass('loading');
          return price.html(data + " 元");
        });
      } else {
        if (days === "") {
          price.html('');
          err.html('');
          price.removeClass('loading');
        } else {
          err.html("请输入整数天数");
        }
        return false;
      }
    };
    day_cn.keyup(function() {
      return cost(day_cn);
    });
    day_us.change(function() {
      return cost(day_us);
    });
    day_cn.trigger("keyup");
    day_us.trigger("change");
    return $('#pay').submit(function() {
      return cost($("#days"));
    });
  });

}).call(this);
