(function() {
  var vm_os, vm_reboot,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  vm_reboot = function(id) {
    return $.fancybox({
      content: "<form id=\"vm_reboot\">\n<input name=\"_xsrf\" style=\"display:none\" value=\"{$.cookie.get(\"_xsrf\")}\">\n<div >请输入42区登录密码确认</div>\n<div ><input type=\"password\" name=\"password\" value=\"\"></div>\n<span><button type=\"submit\">重启 vps " + id + "</button></span>\n</form>",
      afterShow: function() {
        var reboot;
        reboot = $('#vm_reboot');
        return reboot.submit(function() {
          var password;
          password = $('input[name="password"]').val();
          $.post("/j/reboot/" + id, {
            "_xsrf": $.cookie.get("_xsrf"),
            "password": password
          }, function(data) {
            if (__indexOf.call(data, msg) >= 0) {
              return alert(data.msg);
            } else {
              return $('.fancybox-close').trigger('click');
            }
          });
          return false;
        });
      }
    });
  };

  vm_os = function(id) {
    return $.fancybox({
      content: "<div><span>警告</span>:重装将清空除 /root 和 /home 外的所有数据，请做好备份</div>\n<div id=\"vps_os_select\"></div>\n<div >输入42区登录密码确认</div>\n<div ><input type=\"password\" name=\"password\"  ></div>\n<span><button class=\"reinstall\" >vps " + id + " 重装系统 并重置密码</button></span> ",
      afterShow: function() {
        return $('.reinstall').click(function() {
          var password;
          password = $('input[name="password"]').val();
          return $.post("/j/os/" + id, {
            "_xsrf": $.cookie.get("_xsrf"),
            "password": password
          }, function(data) {
            if (__indexOf.call(data, msg) >= 0) {
              alert(data.msg);
            }
            return $('.fancybox-close').trigger('click');
          });
        });
      }
    });
  };

}).call(this);
