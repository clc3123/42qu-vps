(function() {

  window.domain_rm = function(vm_id, id) {
    return $.postJSON("/admin/domain/rm/" + vm_id + "/" + id, function() {
      alert('已删除');
      return location.href = location.href;
    });
  };

  $(function() {
    return $("#record").change(function() {
      var record;
      record = $("#recorded");
      if (this.checked) {
        return record.show();
      } else {
        return record.hide();
      }
    });
  });

}).call(this);
