(function() {

  $(function() {
    return $(".host_update").click(function() {
      var host;
      host = prompt('请输入主机编号');
      if (host) {
        return $.postJSON("/god/vm/host/edit/" + this.rel + "/" + host, function() {
          return location.reload();
        });
      }
    });
  });

  $(function() {
    return $(".days_update").click(function() {
      var days;
      days = prompt('请输入天数');
      if (days) {
        return $.postJSON("/god/vm/days/edit/" + this.rel + "/" + days, function() {
          return location.reload();
        });
      }
    });
  });

  $(function() {
    return $(".bandwidth_update").click(function() {
      var band_width;
      band_width = prompt('请输入带宽');
      if (band_width) {
        return $.postJSON("/god/vm/bandwidth/edit/" + this.rel + "/" + band_width, function() {
          return location.reload();
        });
      }
    });
  });

  window.os = function(vm_id, os_id) {
    return $.getJSON('/j/os', function(os_list) {
      var item, select, _, _i, _len;
      _ = $.html();
      _("<select>");
      for (_i = 0, _len = os_list.length; _i < _len; _i++) {
        item = os_list[_i];
        _("<option ");
        if (item[0] === os_id) {
          _("selected ");
        }
        _("value=\"" + item[0] + "\">" + item[1] + "</option>");
      }
      _("</select>");
      select = _.html();
      return $.fancybox({
        content: "<div id=\"vps_os_select\">" + select + "</div>\n<div><input type=\"checkbox\" id=\"reinstall\">重装系统</div>\n<button class=\"osupdate BTN\" >vps " + vm_id + " 修改系统</button>",
        afterShow: function() {
          return $('.osupdate').click(function() {
            var reinstall;
            os_id = $('#vps_os_select select').val();
            reinstall = 0;
            $('#reinstall').attr('checked') && (reinstall = 1);
            return $.postJSON("/god/vm/os/edit/" + vm_id + "/" + os_id, {
              reinstall: reinstall
            }, function() {
              $('.fancybox-close').trigger('click');
              return location.reload();
            });
          });
        }
      });
    });
  };

}).call(this);
