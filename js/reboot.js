(function() {

  window.reboot = function(id) {
    return $.fancybox({
      content: "<form id=\"vm_reboot\">\n<div style=\"margin-top:27px\">请输入42区登录密码确认</div>\n<div style=\"margin-top:27px\"><input type=\"password\" name=\"password\" value=\"\" style=\"border: 1px solid #CCCCCC;padding: 14px 7px;width: 300px;\"></div>\n<span style=\"margin-top:27px; display:block\"><button type=\"submit\" class=\"BTN\">重启 vps " + id + "</button></span>\n</form>",
      afterShow: function() {
        var reboot;
        reboot = $('#vm_reboot');
        return reboot.submit(function() {
          var password;
          password = $('input[name="password"]').val();
          $.postJSON("/j/reboot/" + id, {
            "password": password
          }, function(data) {
            if (data.msg) {
              alert(data.msg);
            } else {
              alert("正在重启，去喝杯咖啡吧!");
            }
            return $('.fancybox-close').trigger('click');
          });
          return false;
        });
      }
    });
  };

}).call(this);
