#coding:utf-8
from model.vps.template import VmTemplate

import _env
from _route import route
from _base import View

from zweb.view_base import LoginView , JsonLoginView
from model.vps.template import VmTemplate
from model.vps.vm_os import VPS_OS_SHOW, VPS_OS_DICT
from json import dumps
from model.base.user_password import user_password_verify
from model.vps.vm import Vm, vm_reinstall, vm_reboot
from model.vps.saas.cmd import cmd_new, CMD


@route("/j/price/(\d+)/(\d+)")
class price(View):
    def get(self, id, days):
        t = VmTemplate.mc_get(id)
        price = 0
        if t:
            price = t.price(days)
        self.finish(str(price))

@route("/j/os")
class os(View):
    def get(self):
        os = [(id, VPS_OS_DICT[id]) for id in VPS_OS_SHOW]
        self.finish(dumps(os))        

class VmAdminView(JsonLoginView):
    def init(self, vm_id):
        current_user_id = self.current_user_id
        vm = Vm.mc_get(vm_id) 
        password = self.get_argument('password','')
        if not vm.can_admin(current_user_id):
            self.finish({"msg":"无权管理"})
        elif not user_password_verify(current_user_id, password):
            self.finish({"msg":"密码有误"})
        else:
            self.vm = vm


@route("/j/os/(\d+)")
class os_id(VmAdminView):
    def post(self, vm_id):
        os = int(self.get_argument('os',0))
        vm = self.vm
        if os and os in VPS_OS_DICT:
            vm.os_id = os
            vm.save()
            vm_reinstall (vm.id)
        self.finish('{}')

@route("/j/reboot/(\d+)")
class os_id(VmAdminView):
    def post(self, vm_id):
        current_user_id = self.current_user_id
        vm_reboot (vm_id)
        self.finish('{}')

if __name__ == "__main__":
    pass

 
