#!/usr/bin/env python
# coding:utf-8

import _env
from view.vps._route import route
from view.vps.god._base import VpsGodView 
from misc.lib.base.page import page_limit_offset
from zweb.errtip import Errtip
from zweb.lib.jsdict import JsDict
from misc.lib.vps.ip import is_ipv4
from model.vps.host import VpsDataCenter, VpsHost, vps_host_edit, vps_host_new


@route('/god/host/list/(\d+)')
class host_list (VpsGodView): 

    def get (self, data_center_id):
        data_center = VpsDataCenter.get (data_center_id)
        if not data_center:
            raise Exception ("data center not found")
        host_list = VpsHost.where (data_center_id=data_center_id)
        self.render (host_list=host_list, data_center=data_center)
            

@route('/god/host/new')
@route('/god/host/edit/(\d+)')
class host_edit(VpsGodView):

    def get (self, _id=0):
        if _id:
            host = VpsHost.get(_id)
            if not host:
                raise Exception ("not found")
        else:
            host = JsDict()
        self.render(host=host,errtip=Errtip(), EDIT_HOST_ID=_id)

    def post (self,_id=0):
        errtip = Errtip ()
        ip_ext = self.get_argument ('ip_ext', None)
        if not is_ipv4 (ip_ext):
            errtip.ip_ext = "请输入外网ip"
        ip_inner = self.get_argument ('ip_inner', None)
        if not is_ipv4 (ip_inner):
            errtip.ip_inner = "请输入内网ip"
        host_type = self.get_argument ("host_type", None)
        if not host_type:
            errtip.host_type = "请选择主机类型"

        data_center_id = self.get_argument ("data_center", '') 
        ram_count = self.get_argument ("ram_count", '')
        hd_count = self.get_argument ("hd_count", '')
        host_id = self.get_argument ('host_id', '')
        vps_limit = self.get_argument ("vps_limit", '')
        try:
            data_center_id = int (data_center_id)
        except (ValueError, TypeError):
            errtip.data_center = "请选择机房"
        try:
            ram_count = int (ram_count)
        except (ValueError, TypeError):
            errtip.ram_count = "请填写内存总数"
        try:
            hd_count = int (hd_count)
        except (ValueError, TypeError):
            errtip.hd_count = "请填写硬盘总量"

        if _id:
            try:
                vps_limit = int (vps_limit)
            except (ValueError, TypeError):
                errtip.host_id = "请输入vps限制个数"
        else:
            try:
                host_id = int (host_id)
                if not host_id:
                    errtip.host_id = "请输入主机id"
                else:
                    if VpsHost.get (host_id):
                        print "hhahha"
                        errtip.host_id = "主机id已存在"
            except (ValueError, TypeError):
                errtip.host_id = "请输入主机id"


        if not errtip:
            if _id:
                host = vps_host_edit (_id, ip_ext, ip_inner, data_center_id, ram_count, hd_count, host_type, vps_limit)
                self.redirect ("/god/host/list/%d" % (data_center_id))
            else:
                vps_host_new (host_id, ip_ext, ip_inner, data_center_id, ram_count, hd_count, host_type)
                self.redirect ("/god/host/list/%d" % (data_center_id))
        else:
            host = JsDict(dict(ip_ext_str=ip_ext, ip_inner_str=ip_inner, host_id=host_id, host_type=host_type, 
                    data_center_id=data_center_id, ram_count=ram_count, hd_count=hd_count))
            self.render(host=host, EDIT_HOST_ID=_id, errtip=errtip) 

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 :
