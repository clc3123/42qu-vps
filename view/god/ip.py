#!/usr/bin/env python
# coding:utf-8

import _env
from view.vps._route import route
from view.vps.god._base import VpsGodView 
from misc.lib.base.page import page_limit_offset
from zweb.errtip import Errtip
from zweb.lib.jsdict import JsDict
from misc.lib.vps.ip import is_ipv4, ip2int, int2ip, check_host_ipv4, check_gateway_ipv4
from model.vps.datacenter import VpsDataCenter
from model.vps.ip import VpsIp, vps_ip_new, vps_ip_list, vps_ip_count, vps_ip_rm
from model.vps.ip_inner import VpsIpInner, vps_ip_inner_new, vps_ip_inner_list, vps_ip_inner_count, vps_ip_inner_rm
def _ip_post (self, data_center_id, ip_type, link, vm_id=0):
    errtip = Errtip ()
    try:
        data_center_id = int (data_center_id)
    except (ValueError, TypeError):
        errtip.data_center = '请选择机房'
    ip_begin = self.get_argument('ip_begin','')
    if not is_ipv4 (ip_begin):
        errtip.ip_begin = "请输入起始ip XXX.XXX.XXX.XXX"
    ip_end = self.get_argument('ip_end', '')
    if ip_end and not is_ipv4 (ip_end):
        errtip.ip_end = "ip格式不对"
    netmask = self.get_argument('netmask','')
    if not is_ipv4 (netmask):
        errtip.netmask = "请输入掩码XXX.XXX.XXX.XXX"
    gateway = self.get_argument('gateway','')
    if not is_ipv4 (gateway):
        errtip.gateway  = "请输入网关XXX.XXX.XXX.XXX"
    if not errtip:
        if not check_host_ipv4 (ip2int(ip_begin), ip2int (netmask)):
            errtip.ip_begin = 'ip或掩码错误'
        elif ip_begin == gateway or not check_gateway_ipv4 (ip2int (ip_begin), ip2int (netmask), ip2int (gateway)):
            errtip.ip_begin = 'ip或网关或掩码错误'
        if ip_end:
            if not check_host_ipv4 (ip2int (ip_end), ip2int (netmask)):
                errtip.ip_end = 'ip或掩码错误'
            elif ip_end == gateway or not check_gateway_ipv4 (ip2int (ip_end), ip2int (netmask), ip2int (gateway)):
                errtip.ip_end = 'ip或网关或掩码错误'
            elif ip2int (ip_end) < ip2int (ip_begin):
                errtip.ip_end = '结束ip不能小于开始ip'
            elif ip2int (ip_end) - ip2int (ip_begin) > 255:
                errtip.ip_end = '一次添加的网段不能太大'
    
    if not errtip:
        if not ip_end:
            ip_end = ip_begin
        ip_begin = ip2int(ip_begin)
        ip_end  = ip2int(ip_end)
        netmask = ip2int (netmask)
        gateway = ip2int (gateway)
        if int(ip_type) == 1:
            vps_ip_new(data_center_id,vm_id, gateway,  netmask, ip_begin, ip_end)
        else:
            vps_ip_inner_new(data_center_id,vm_id, gateway, netmask, ip_begin, ip_end)
        self.redirect ('%s' % (link))
    else:
        ip = JsDict ({
            'data_center_id':data_center_id, 
            'ip_begin':ip_begin, 
            'ip_end':ip_end, 
            'netmask':netmask, 
            'gateway':gateway,
            'data_center_id':data_center_id})
        self.render (ip=ip, errtip=errtip)

@route ('/god/ip/new(/(\d+))?')
class ip_new (VpsGodView):

    def get (self, _x=0, data_center_id=0):
        errtip = Errtip ()
        data_center_id=data_center_id and int(data_center_id) or 0
        ip = JsDict ({'data_center_id':data_center_id})
        self.render (ip=ip, errtip=errtip)

    def post(self, _x=0, _x2=0):
        data_center_id = self.get_argument ('data_center', 0)
        ip_type= self.get_argument('ip_type', 1)
        link ='/god/ip/%s/list/%s'%(ip_type, data_center_id)
        _ip_post(self, data_center_id, ip_type, link)


@route ('/god/ip/(\d+)/rm/(\d+)')
class ip_rm (VpsGodView):

    def post (self, ip_type, ip_id):
        try:
            if int(ip_type) == 1:
                res = vps_ip_rm(int(ip_id))
            else:
                res = vps_ip_inner_rm(int(ip_id))
            self.finish ({})
        except Exception , e:
            print str(e)


@route ('/god/ip/(\d+)/list/(\d+)(/(\d+))?')
class ip_list (VpsGodView):

    def get (self, ip_type, data_center_id=0, x=None, offset=0):
        data_center_id=data_center_id and int(data_center_id) or 0
        datacenter  = VpsDataCenter.get (data_center_id)
        if int(ip_type) == 1:
            count = vps_ip_count (data_center_id)
            page, limit, offset = page_limit_offset (
                    '/god/ip/%s/list/%s/%%s'% (ip_type, data_center_id),
                    count,
                    offset,
                    100
                    )
            ol = vps_ip_list(data_center_id, limit, offset)
        else:
            count = vps_ip_inner_count (data_center_id)
            page, limit, offset = page_limit_offset (
                    '/god/ip/%s/list/%s/%%s'% (ip_type, data_center_id),
                    count,
                    offset,
                    100
                    )
            ol = vps_ip_inner_list(data_center_id, limit, offset)
        return self.render(
                find_ip='',
                ip_type = ip_type,
                datacenter=datacenter,
                ol = ol,
                page = page,
                )

    def post (self, ip_type, data_center_id=0, x=None, offset=0):
        find_ip_str = self.get_argument ("find_ip", "")
        try:
            find_ip = ip2int(find_ip_str)
        except Exception:
            find_ip = 0
        if int(ip_type) == 1:
            ol = VpsIp.where(ip=find_ip)
        else:
            ol = VpsIpInner.where(ip=find_ip)
        page, limit, offset = page_limit_offset (
                '/god/ip/%s/list/%s/%%s'%(ip_type, data_center_id),
                1,
                0,
                100
                )

        print ol
        if len(ol) == 1:
            datacenter = VpsDataCenter.get (ol[0].data_center_id)
        else:
            datacenter = VpsDataCenter.get (data_center_id)
        self.render (
                ip_type=ip_type,
                page=page,
                datacenter=datacenter,
                ol=ol,
                find_ip=find_ip_str
                )
        



# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 :
