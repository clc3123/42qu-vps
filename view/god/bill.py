#coding:utf-8

import _env
from view.vps._route import route
from _base import VpsGodView
from model.base.gid import CID 
from model.vps.bill import bill_list_cid, bill_list_by_month 
from model.vps.const import CID_BILL 
from misc.lib.base.timetool import today_days, days2today


@route("/god/bill")
class index(VpsGodView):
    def get(self):
        bill_list =None
        self.render(bill_list=bill_list)


@route("/god/bill/(\d+)")
class bill_list(VpsGodView):
    def get(self,cid):
        bill_list =bill_list_cid(cid)
        for bill in bill_list:
            bill.things = CID_BILL[bill.cid](bill.tid)
        self.render(bill_list=bill_list)


@route("/god/bill/month/(\d+)")
class bill_month(VpsGodView):
    def get(self, day):
        day = int(day)
        days = today_days() 
        days = days2today(days -1)
        bill_list = bill_list_by_month(day)
        for bill in bill_list:
            bill.things = CID_BILL[bill.cid](bill.tid)
        self.render(days=days, bill_list=bill_list)
        pass

@route("/god/bill/month")
class index(VpsGodView):
    def get(self):
        bill_list =None
        self.render(bill_list=bill_list)

@route("/god/bill/month/(\d+)/(\d+)")
class bill_month(VpsGodView):
    def get(self, day):
        day = int(day)
        days = today_days() 
        days = days2today(days -1)
        bill_list = bill_list_by_month(day)
        for bill in bill_list:
            bill.things = CID_BILL[bill.cid](bill.tid)
        self.render(days=days, bill_list=bill_list)



if __name__ == "__main__":

    pass
