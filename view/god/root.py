
# coding:utf-8

import _env
from view.vps._route import route
from view.vps.god._base import VpsGodView 
from model.vps.datacenter import VpsDataCenter
from zweb.errtip import Errtip


@route('/god/datacenter')
class datacenter(VpsGodView):

    def get (self):
        self.render()

@route('/god/datacenter/toggle_full/(\d+)')
class datacenter(VpsGodView):

    def get (self, _id=0):
        if _id:
            datacenter = VpsDataCenter.get (_id)
            if datacenter:
                if datacenter.is_full:
                    datacenter.is_full = 0
                else:
                    datacenter.is_full = 1
                datacenter.save ()
        self.redirect ('/god/datacenter')


@route('/god')
class index(VpsGodView):
    def get (self):
        self.render()
