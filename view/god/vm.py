#coding:utf-8

import _env
from view.vps._route import route
from _base import VpsGodView
from misc.lib.base.page import page_limit_offset
from model._db import redis, redis_key
from model.user.search import user_search
from model.base.user import User
from model.vps.datacenter import VpsDataCenter
from model.vps.vm import vm_new, Vm, vm_list, vm_state_count, vm_open, vm_reinstall, vm_upgrade_template, vm_search as _vm_search
from misc.lib.vps.ip import is_ipv4, ip2int
from model.vps.host import vps_host_bind_vm, vps_host_rm_vm
from model.vps.ip import vps_ip_bind, vps_ip_unbind
from model.vps.ip_inner import vps_ip_inner_bind, vps_ip_inner_unbind
from model.vps.template import VmTemplate
from model.vps.bank import vm_cost_new
from model.vps.const import VM_STATE, VM_IP_TYPE
from model.vps.vm_pay import vm_payed, vm_renew
from misc.lib.base.timetool import today_days
from view.vps.god.ip import _ip_post
from zweb.errtip import Errtip
from zweb.lib.jsdict import JsDict
from model.vps.template import VmTemplate
from json import dumps

@route("/god/vm/(\d+)")
class vm_id(VpsGodView):
    def get(self, id):
        vm = Vm.mc_get(id)
        if not vm:
            return
        template = VmTemplate.mc_get(vm.vm_template_id)
        self.render(vm=vm, template=template)

LIMIT = 100
@route("/god/vm")
@route("/god/vm/list(\d+)-(\d+)(-(\d*))?")
class index(VpsGodView):
    def get(self, state=VM_STATE.OPEN, n=1, _x=None, host_id=''):
        if not host_id:
            host_id = None
        count = vm_state_count(host_id=host_id) 
        page, limit, offset = page_limit_offset (
                '/god/vm/list%s-%%s'%state, 
                count[int(state)],
                n,
                LIMIT 
        )
        _vm_list = vm_list(limit, offset, state, host_id)
        selected_host = host_id is not None and host_id or ''
        self.render(page = page, vm_list=_vm_list, state_count=count, selected_state=state, selected_host=selected_host)

@route("/god/vm/open/(\d+)")
class open_vm (VpsGodView):

    def get (self, vm_id):
        vm_open (vm_id)
        self.redirect ("/god/vm/%s" % (vm_id))

@route("/god/vm/open/mail/(\d+)")
class open_vm_mail(VpsGodView):

    def get(self, vm_id):
        vm = Vm.mc_get(vm_id)
        vm.state = VM_STATE.OPEN
        vm.save ()
        self.redirect('/god/vm/%s'% (vm_id))

@route("/god/vm/close/(\d+)")
class close_vm (VpsGodView):

    def get (self, vm_id):
        vm = Vm.get (vm_id)
        if vm:
            vm.state = VM_STATE.CLOSE
            vm.save ()
            self.redirect ("/god/vm/%s" % (vm_id))


@route("/god/vm/delete/(\d+)")
class delete_vm (VpsGodView):

    def get (self, vm_id):
        vm = Vm.get (vm_id)
        if vm:
            vm.state = VM_STATE.RM
            vm.save ()
        self.redirect ("/god/vm/%s" % (vm_id))


@route("/god/vm/new")
class new(VpsGodView):
    def get(self):
        self.render()

    def post(self):
        q = self.get_argument('q', '')
        user_id = user_search(q)
        if user_id:
            return self.redirect("/god/vm/new/%s"%user_id)
        self.render(q=q)

@route("/god/vm/new/(\d+)")
class new_id(VpsGodView):
    def get(self, user_id):
        self.render(user=User(user_id))


@route("/god/vm/new/(\d+)/(\d+)")
class new_id_host(VpsGodView):
    def get(self, user_id, data_center_id):
        data_center = VpsDataCenter.get (data_center_id)
        self.render(
            user=User(user_id), 
            data_center=data_center
        )

    def post(self, user_id, data_center_id):
        
        day = int(self.get_argument('day', 4))
        host = int(self.get_argument('host'))
        os = int(self.get_argument('os'))
        template_id = int(self.get_argument('template'))
        ip_type = self.get_argument('ip_type', 1)
        template = VmTemplate.mc_get(template_id)
        data_center_id = int(data_center_id)
        user_id = int(user_id)
        vm = vm_new(
            user_id, 
            template_id,
            os,
            data_center_id,
            day,
            host_id=host,
            ip_type=ip_type
        )
        vm_payed(vm.id)
        if int(ip_type) == VM_IP_TYPE.HAS_IP_INT_ONLY:
            vps_ip_unbind(vm.id)
        return self.redirect("/god/vm/%s"%vm.id)

@route("/god/vm/ip/rm/(\d+)/(\d+)")
class rm_ip(VpsGodView):
    def get (self, vm_id, ip_inner):
        vm = Vm.get (vm_id)
        if not vm or not ip_inner:
            return
        vm_id = int(vm_id)
        ip_inner = int(ip_inner)
        vps_ip_unbind (vm_id, ip_inner)
        vps_ip_inner_unbind (vm_id, ip_inner)
        return self.redirect("/god/vm/%s"%vm.id)


@route("/god/vm/ip/new/(\d+)")
class new_ip(VpsGodView):
    def get(self, vm_id):
        errtip = Errtip ()
        vm = Vm.get (vm_id) 
        if not vm:
            return
        self.render (vm=vm, ip='', errtip=errtip)

    def post(self,vm_id):
        vm = Vm.get (vm_id) 
        if not vm:
            return
        errtip = Errtip ()
        ip = self.get_argument('ip',0)
        ip_type= int(self.get_argument ('ip_type', 0))
        if not ip or not is_ipv4 (ip): 
            errtip.ip = "请输入ip" 
        try:
            ip = ip2int (ip)
            vps_ip_inner_bind(vm_id, ip)
            if ip_type == VM_IP_TYPE.HAS_IP_EXT:
                vps_ip_bind(vm_id, ip)
        except Exception, e:
            errtip.ip = str(e)
        if not errtip:
            return self.redirect("/god/vm/%s"%vm.id)
        else:
            self.render (vm=vm, ip=ip, errtip=errtip)

@route("/god/vm/search")
class vm_search(VpsGodView):
    def get(self):
        q = self.get_argument('q', '')
        if q:
            vm_list=_vm_search(q)
        else:
            vm_list = None        
        self.render(vm_list=vm_list, q=q)




@route("/god/vm/host/edit/(\d+)/(\d+)")
class vm_edit_host(VpsGodView):

    def post(self, vm_id, host_id):
        vm = Vm.get (vm_id)
        if vm and vm.state_ in [VM_STATE.PAY, VM_STATE.OPEN, VM_STATE.CLOSE] :
            vps_host_bind_vm (host_id, vm)
    
@route("/god/vm/os/edit/(\d+)/(\d+)")
class vm_edit_os(VpsGodView):

    def post(self, vm_id, os_id):
        reinstall = int(self.get_argument('reinstall',''))
        vm = Vm.get (vm_id)
        if vm:
            vm.os_id = os_id 
            vm.save ()
        if reinstall == 1:
            vm_reinstall (vm_id)            

@route("/god/vm/datacenter/edit/(\d+)")
class vm_edit_datacenter(VpsGodView):

    def get(self, vm_id):
        vm = Vm.get(vm_id)
        if not vm:
            return
        self.render(vm=vm)

    def post(self, vm_id):
        datacenter_id = self.get_argument('datacenter', '')
        vm = Vm.get (vm_id)
        if vm and vm.state_ in [VM_STATE.PAY, VM_STATE.OPEN, VM_STATE.CLOSE]:
            vm.data_center_id = datacenter_id
            vm.save ()
            vps_host_rm_vm (vm)
        return self.redirect("/god/vm/%s" % vm_id)

@route("/god/vm/template/edit/(\d+)")
class vm_edit_template(VpsGodView):

    def get(self,vm_id):
        vm = Vm.get(vm_id)
        if not vm:
            return
        self.render(vm=vm)

    def post(self,vm_id):
        template_id = self.get_argument('template','')
        vm_upgrade_template (vm_id, template_id)
        return self.redirect("/god/vm/%s" % vm_id)

@route("/god/vm/days/edit/(\d+)/(\d+)")
class vm_renew_days(VpsGodView):
    
    def post(self, vm_id, days):
        vm_renew(vm_id, int(days))

@route("/god/vm/bandwidth/edit/(\d+)/(\d+)")
class vm_renew_bandwidth(VpsGodView):
    
    def post(self, vm_id, bandwidth):
        vm = Vm.get(vm_id)
        if vm:
            vm.bandwidth=bandwidth
            vm.save ()

if __name__ == "__main__":

    pass
