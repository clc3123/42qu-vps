# -*- coding: utf-8 -*-
import _env
from model.base.gid import CID
from view.base.god import GodView, JsonGodView as _JsonGodView
from zweb.view_base import LoginView, View, JsonLoginView
import model.vps.god

class VpsGodView(GodView):
    CID = CID.VPS

