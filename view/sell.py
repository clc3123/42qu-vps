#coding:utf-8

import _env
from _route import route
from _base import View
from zweb.errtip import Errtip
from misc.config import HOST
from model.base.gid import CID
from model.auth.reg import auth_reg_user_new_with_mail
from model.base.user_mail import user_id_by_mail
from model.base.user_password import user_password_new, user_password_verify
from model.base.mail import RE_MAIL
from view.auth._util.login import login as _login
from model.vps.template import VmTemplate
from model.vps.vm import vm_new, Vm 
from model.vps.const import VM_STATE, VM_STATE_CN
from model.pay.pay import pay_order_new
from model.vps.vm_os import VPS_OS_SHOW, VPS_OS_DICT
from model.vps.datacenter import VPS_COUNTRY_ID2NAME, VpsDataCenter
from misc.lib.base.timetool import today_days
from model.vps.vm_pay import vm_payed
from model.pay.pay import payed_order_get
from model.vps.reward import vps_reward_pay
from model.pay.const import PAY_ORDER_LINK
from zweb.view_base import View, LoginView

PAY_ORDER_LINK[CID.VPS] = "//vps.%s/payed/%%s"%HOST
PAY_ORDER_LINK[CID.VPS_VM_RENEW] = "//vps.%s/admin"%HOST



@route("/renew/(\d+)")
class renew(LoginView):
    def init(self, id):
        vm = Vm.mc_get(id)
        if vm.state >= VM_STATE.PAY:
            self.vm = vm
        else:
            self.redirect("/admin")

    def get(self, id):
        self.render(vm=self.vm)

    def post(self, id):
        vm = self.vm
        days = max(int(self.get_argument('days',1)),1)
        user_id = self.current_user_id
        price = vm.template.price(days)
        order = pay_order_new(
            CID.VPS_VM_RENEW , vm.id, price*100, days, user_id, days
        ) 
        self.redirect(order.pay_url)
         

@route("/payed/(\d+)")
class payed(View):
    def get(self, id):
        order = payed_order_get(id, CID.VPS)
        if not order:
            return self.redirect("/")
        vps_reward_pay (self.current_user_id, order.id, order.cent, self.xsrf_token)
        self.render(order=order)

@route("/sell/(\d+)/(\d+)")
class index(View):
    def get(self, template_id, days):
        self.render(errtip=Errtip(), days=days, template=VmTemplate.mc_get(template_id))

    def post(self, template_id, days):
        errtip = Errtip()
        template=VmTemplate.mc_get(template_id)
        user_id = self.current_user_id
        if not user_id:
            mail= self.get_argument('mail', '')
            password= self.get_argument('password', '')
            name = self.get_argument('name', '')
            sex = int(self.get_argument('sex', 0))
            
            login = False

            user_id = user_id_by_mail(mail)
            if user_id: 
                if user_password_verify(user_id, password):
                    login = True
                else:
                    errtip.password = "帐号已注册 , 但密码有误"
            elif not mail:
                errtip.mail= "请填写邮箱"
            elif not name:
                errtip.name= "请填写用户名"
            elif not password:
                errtip.password= "请填写密码"
            elif not RE_MAIL.match(mail):
                pass
            else:



                user_id = auth_reg_user_new_with_mail(mail, CID.USER, rid =0, password=password, name=name, sex=sex)
                login = True
        
            if login:
                _login(self, user_id)
            else:
                return self.render(
                    errtip=errtip,
                    mail = mail,
                    password = password,
                    name = name,
                    sex = sex,
                    days = days,
                    template=template
                )
        if user_id:
            os = int(self.get_argument('os', VPS_OS_SHOW[0]))
            if os not in VPS_OS_DICT:
                os = VPS_OS_SHOW[0]
            days = max(int(self.get_argument('days', 1)),1)
            data_center_id = int(self.get_argument('data_center', 0))
            datacenter = VpsDataCenter.mc_get (data_center_id)
            vm = vm_new(
                user_id, 
                template_id, 
                os, 
                data_center_id, 
                days,
            )

            if datacenter and datacenter.country_id == template.country:
                price = template.price(days)
                order = pay_order_new(CID.VPS, vm.id, price*100, 0, user_id, 0)
 
                return self.redirect(order.pay_url)
 
        self.redirect("/") 





