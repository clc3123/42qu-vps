#coding:utf-8
import _env
from _route import route
from _base import View, LoginView
from view.user._base import  JsonLoginView
from model.vps.reward import reward_by, vps_reword_by_user_id
from model.base.host import id_by_host_prefix
from zweb.errtip import Errtip

@route("/")
class index(View):
    def get(self):
        self.render()

    def post(self):
        mail = self.get_argument('mail','')
        self.render(mail=mail)

@route("/by/([^/]+)")
@route("/by/([^/]+)/(.*)")
class by(View):
    def get(self, url, via=''):
        by_id = id_by_host_prefix(url)
        user_id = self.current_user_id
        referer = self.request.headers.get('Referer',"")
        reward_by(user_id, self.xsrf_token, by_id, referer)
        return self.redirect("/")

@route("/link")
class link(LoginView):
    def get(self):
        self.render()

@route("/link/bill")
class bill(LoginView):
    def get(self):
        bill_list = vps_reword_by_user_id(self.current_user_id)
        self.render('/vps/root/bill.html', bill_list=bill_list)


if __name__ == "__main__":
    pass

