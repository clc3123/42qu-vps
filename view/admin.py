#coding:utf-8
import _env
from _route import route
from zweb.errtip import Errtip
from _base import LoginView, NoLoginView, VmAdminView, JsonVmAdminView
from model._db import Model , ModelMc, redis, redis_key, McLimitA, McNum
from model.vps.vm import Vm, vm_transfer
from misc.lib.base.tld_name import tld_name 
from model.vps.const import VM_DOMAIN_STATE
from model.vps.domain import VmDomain, domain_bind_new, domain_rm_if_can
from model.vps.mail import remind_mail_new, remind_mail_renew, remind_mail_verifyed, remind_mail_del, remind_mail_list
from model.user.search import user_search

@route("/me")
@route("/admin")
@route("/list")
class admin_index(LoginView):
    def get(self):
        self.render()
    
@route("/admin/(\d+)")
class admin(VmAdminView):
    def get(self, id):
        vm = Vm.mc_get(id)
        if not vm.can_admin(self.current_user_id):
            self.redirect("/admin")
        self.render(vm=vm)

@route("/admin/domain/(\d+)")
class admin_domain(VmAdminView):
    def get(self, vm_id):
        self.render(errtip=Errtip(), vm_id=vm_id)

    def post(self, vm_id):
        errtip = Errtip()
        domain= self.get_argument('domain','')
        #ip= self.get_argument('ip','')
        record= self.get_argument('record',0)
        recorded= self.get_argument('recorded','')
        
        if int(record) == 1:
            state = VM_DOMAIN_STATE.WAIT_VERIFY
        else:
            state = VM_DOMAIN_STATE.UNRECORD
        
        if (domain.startswith('http://') or domain.startswith('https://')):
            domain = domain.split('//')
            domain = str(''.join(domain[1:]))

        domain=str(tld_name(domain))
        if not domain:
            errtip.domain= "请填写正确的域名格式，如42qu.com"
        elif VmDomain.where(domain=domain).count():
            errtip.domain= "域名已绑定"
        else:
            domain_bind_new(vm_id, domain, record, state)
        self.render(errtip=errtip, vm_id=vm_id)

@route("/admin/domain/rm/(\d+)/(\d+)")
class admin_domain_rm(JsonVmAdminView):
    def post(self, vm_id, id):
        domain_rm_if_can(id, self.current_user_id)
        self.finish("{}")

@route("/admin/mail/new/(\d+)/(.*)")
class admin_mail_new(VmAdminView):
    def post(self, vm_id, mail):
        remind_mail_new(vm_id, mail)

@route("/admin/mail/renew/(\d+)/(.*)")
class admin_mail_renew(JsonVmAdminView):
    def post(self, vm_id, mail):
        remind_mail_renew(vm_id, mail)

@route("/admin/mail/rm/(\d+)/(\d+)")
class admin_mail_del(JsonVmAdminView):
    def post(self, vm_id, mail_id):
        remind_mail_del(vm_id, mail_id)

@route("/admin/(\d+)/user/(.*)")
class admin_vm_user(JsonVmAdminView):
    def get(self, vm_id, query):
        user_id = user_search(query)
        self.finish(str(user_id))

@route("/admin/(\d+)/transfer/(\d+)")
class admin_transfer(JsonVmAdminView):
    def post(self, vm_id, user_id):
        vm_transfer(vm_id, int(user_id))

if __name__ == '__main__':
    #remind_mail_new(11, '2807229521@qq.com')
    pass
