# -*- coding: utf-8 -*-
from zweb.view_base import View, LoginView, NoLoginView
from model.vps.vm import Vm

class VmAdminView(LoginView):
    def init(self, vm_id, *args):
        self.vm = vm = Vm.mc_get(vm_id) 
        if not vm.can_admin(self.current_user_id):
            self.redirect("/")


class JsonVmAdminView(LoginView):
    def init(self, vm_id, *args):
        self.vm = vm = Vm.mc_get(vm_id) 
        if not vm.can_admin(self.current_user_id):
            self.finish('{"login":1}')

