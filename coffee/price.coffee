$ ->
    day_cn = $('input[name="days"]')
    price = $('.price')
    err = $('.day_err') 
    type = $('#template').val()

    day_us = $('select[name="days"]')
    re = /^[0-9]+$/

    day_cn.focus()
    cost  = (day)->
        days = $.trim day.val()
        price.html ''
        price.addClass "loading"

        if re.test(days)
            err.html ''
            $.get(
               "/j/price/#{type}/#{days}",
               (data)->
                    price.removeClass 'loading'
                    price.html data + " 元"
            )
        else
            if days == ""
                price.html('')
                err.html ''
                price.removeClass 'loading'
            else
                err.html "请输入整数天数" 
            return false
 
    day_cn.keyup ->
        cost(day_cn) 
    day_us.change ->
        cost(day_us)

    day_cn.trigger "keyup"
    day_us.trigger "change"
    $('#pay').submit ->
        return cost($("#days"))
