window.reboot = (id)->
    $.fancybox(
        content:"""
        <form id="vm_reboot">\
        <div style="margin-top:27px">请输入42区登录密码确认</div>\
        <div style="margin-top:27px"><input type="password" name="password" value="" style="border: 1px solid #CCCCCC;padding: 14px 7px;width: 300px;"></div>\
        <span style="margin-top:27px; display:block"><button type="submit" class="BTN">重启 vps #{id}</button></span>\
        </form>
        """,
        afterShow: ->
               reboot = $('#vm_reboot')
               reboot.submit ->
                    password = $('input[name="password"]').val()
                    $.postJSON(
                        "/j/reboot/#{id}",
                        "password":password,
                        (data)->
                            if data.msg
                                alert data.msg
                            else 
                                alert "正在重启，去喝杯咖啡吧!"
                            $('.fancybox-close').trigger 'click'
                    ) 
                    false
    )

    
