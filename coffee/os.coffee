window.os = (id)->
     $.getJSON(
        '/j/os',
        (os_list)->
            _ = $.html()
            _ """
            <select>
            <option value="0">-</option>
            """
            for item in os_list
                _ """
                   <option value="#{item[0]}">#{item[1]}</option>
                  """
            _ """
              </select>
              """
            select = _.html()
            $.fancybox(
                content:"""
<div style="font-size: 16px;margin-top: 27px;"><span style="color:red">警告</span>:重装将清空除 /root 和 /home 外的所有数据，请做好备份</div>
<div id="vps_os_select">#{select}</div>
<div style="margin-top: 27px;margin-top: 27px;">输入42区登录密码确认</div>
<div style="margin-top: 27px;"><input type="password" name="password"style="padding:14px 7px;border:1px solid #ccc;width:300px" ></div>
<span style="border: 1px solid #CCCCCC;display: inline-block;height: 42px;margin-top: 27px;"><button class="reinstall BTN" >vps #{id} 重装系统 并重置密码</button></span>
                """,
                afterShow: ->
                    $('.reinstall').click ->
                        password = $('input[name="password"]').val()
                        os_id = $('#vps_os_select select').val()
                        $.postJSON( 
                            "/j/os/#{id}",
                            "password":password
                            "os":os_id,
                            (data)->
                                if data.msg
                                    alert data.msg
                                else
                                    alert "正在重装系统，去喝杯咖啡吧！"
                                $('.fancybox-close').trigger 'click'
                        )
                )
     )




$ ->
    $(".mail_new").click ->
        mail = prompt('请输入邮箱,并提醒该用户验证邮箱！')
        if mail 
            $.postJSON("/admin/mail/new/#{this.rel}/#{mail}",->
                location.reload()
            )

window.mail_del = (vm_id, mail_id) ->
    $.postJSON("/admin/mail/rm/#{vm_id}/#{mail_id}",->
        alert '已删除'
        location.reload()
    )

window.mail_renew = (vm_id, mail) ->
    $.postJSON("/admin/mail/renew/#{vm_id}/#{mail}",->
        alert '已重新发送'
        location.reload()
    )

window.vm_transfer = (vm_id, query) ->
    query = prompt '请输入转让给的用户邮箱'
    if query
        $.getJSON(
            "/admin/#{vm_id}/user/#{query}", 
            (user_id)->
                if user_id == 0 or not user_id
                    alert  "该用户不存在！"
                else
                    $.postJSON(
                        "/admin/#{vm_id}/transfer/#{user_id}", ->
                            location.reload()
                    )
        )
